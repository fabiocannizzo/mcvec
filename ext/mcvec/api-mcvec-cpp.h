#pragma once

namespace mcvec {

namespace Private
{
#include "mcvec-c.h"

    template <typename T>
    struct base_num_vec_t
    {
        base_num_vec_t(T *p) : p(p) {}
        operator T*() { return p; }
    private:
        T *p;
    };

    template <typename T>
    struct base_log_vec_t
    {
        base_log_vec_t(T *p) : p(p) {}
        operator T*() { return p; }
    private:
        T *p;
    };


} // namespace Private


enum code {
#include "auto/una-enum.h"
#include "auto/bin-enum.h"
#include "auto/ter-enum.h"
    code_end
};

enum mode {
#include "auto/all-modes.h"
    mode_end
};

typedef Private::base_num_vec_t<const double> c_num_vec_t;
typedef Private::base_num_vec_t<double> num_vec_t;

typedef Private::base_log_vec_t<const double> c_log_vec_t;
typedef Private::base_log_vec_t<double> log_vec_t;


typedef Private::num_cst_t num_cst_t;
typedef Private::log_cst_t log_cst_t;
typedef Private::vec_size_t vec_size_t;

template <code, mode>
struct Op;

#ifndef FORCE_INLINE
#define FORCE_INLINE inline
#endif

inline const char *instr_set()
{
    return Private::mcvec_instr_set();
}

#include "gen-fun-wrappers.h"

} // namespace mcvec
