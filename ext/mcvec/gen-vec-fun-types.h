#include "vec-names.h"
#define MCVEC_FUN(n,m,...) \
template <>\
struct OuterPtrTraits<MCVEC_ENUMNAME(n,m)> {\
   typedef void (*type_t)(vec_size_t, double *, __VA_ARGS__);\
};


#define MCVEC_FUN1(n,m,body,rt,at) MCVEC_FUN(n,m,at)
#define MCVEC_FUN2(n,m,body,rt,at,bt) MCVEC_FUN(n,m,at,bt)
#define MCVEC_FUN3(n,m,body,rt,at,bt,ct) MCVEC_FUN(n,m,at,bt,ct)
#include "auto/all-fun.h"
