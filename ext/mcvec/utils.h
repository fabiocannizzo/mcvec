#pragma once

#include <cstdint>
using std::int64_t;

inline int64_t d2i(double x) { return reinterpret_cast<int64_t&>(x); }
inline double i2d(int64_t x) { return reinterpret_cast<double&>(x); }
inline double b2d(bool f) { return i2d(f ? -1 : 0); }

