#include "names.h"
#define MCVEC_FUN(n,m,rt,...) MCVEC_DLL MCVEC_INDENT(0)void MCVEC_FUNNAME(n,m)(vec_size_t, rt dst, __VA_ARGS__);MCVEC_INDENT(0)
#define MCVEC_FUN1(n,m,body,rt,at) MCVEC_FUN(n,m,rt,at x1)
#define MCVEC_FUN2(n,m,body,rt,at,bt) MCVEC_FUN(n,m,rt,at x1, bt x2)
#define MCVEC_FUN3(n,m,body,rt,at,bt,ct) MCVEC_FUN(n,m,rt,at x1, bt x2, ct x3)

#include "auto/all-fun.h"
