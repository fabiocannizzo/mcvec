// This is an auto generated file

#ifndef MCVEC_FUN1
#    error MCVEC_FUN1 is not defined
#endif
#ifndef MCVEC_FUN2
#    error MCVEC_FUN2 is not defined
#endif
#ifndef MCVEC_FUN3
#    error MCVEC_FUN3 is not defined
#endif
#ifndef MCVEC_INDENT
#    define MCVEC_INDENT(n)
#endif

/* absolute value */
MCVEC_FUN1(abs, v, { MCVEC_INDENT(8)CALL(and, vc, x1, i2d(0x7FFFFFFFFFFFFFFF));MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t)

/* x1 + x2 + x3 */
MCVEC_FUN3(add3, ccv, { MCVEC_INDENT(8)FWD(add, vc, x3, x1+x2);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, num_cst_t, c_num_vec_t)

/* x1 + x2 + x3 */
MCVEC_FUN3(add3, cvc, { MCVEC_INDENT(8)FWD(add, vc, x2, x1+x3);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t, num_cst_t)

/* x1 + x2 + x3 */
MCVEC_FUN3(add3, cvv, { MCVEC_INDENT(8)FWD(add3, vvc, x2, x3, x1);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t, c_num_vec_t)

/* x1 + x2 + x3 */
MCVEC_FUN3(add3, vcc, { MCVEC_INDENT(8)FWD(add, vc, x1, x2+x3);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t, num_cst_t)

/* x1 + x2 + x3 */
MCVEC_FUN3(add3, vcv, { MCVEC_INDENT(8)FWD(add3, vvc, x1, x3, x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t, c_num_vec_t)

/* x1 + x2 + x3 */
MCVEC_FUN3(add3, vvc, { MCVEC_INDENT(8)if(x3!=0.0) MCVEC_INDENT(12)CALL(add3, vvc, x1, x2, x3); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(add,vv,x1,x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t, num_cst_t)

/* x1 + x2 + x3 */
MCVEC_FUN3(add3, vvv, { MCVEC_INDENT(8)CALL(add3, vvv, x1, x2, x3);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t, c_num_vec_t)

/* addition */
MCVEC_FUN2(add, cv, { MCVEC_INDENT(8)FWD(add,vc,x2,x1);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t)

/* addition */
MCVEC_FUN2(add, vc, { MCVEC_INDENT(8)if(x2!=0.0) MCVEC_INDENT(12)CALL(add, vc, x1, x2); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(copy,v,x1);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t)

/* addition */
MCVEC_FUN2(add, vv, { MCVEC_INDENT(8)CALL(add, vv, x1, x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t)

/* logical and */
MCVEC_FUN2(and, cv, { MCVEC_INDENT(8)FWD(and,vc,x2,x1);MCVEC_INDENT(4)}, log_vec_t, log_cst_t, c_log_vec_t)

/* logical and */
MCVEC_FUN2(and, vc, { MCVEC_INDENT(8)if(x2) MCVEC_INDENT(12)CALL(copy,v,x1); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(set,c,0.0);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t, log_cst_t)

/* logical and */
MCVEC_FUN2(and, vv, { MCVEC_INDENT(8)CALL(and, vv, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t, c_log_vec_t)

/* not x1 and x2 */
MCVEC_FUN2(andnot1, cv, { MCVEC_INDENT(8)if(x1) MCVEC_INDENT(12)CALL(set,c,false); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(copy,v,x2);MCVEC_INDENT(4)}, log_vec_t, log_cst_t, c_log_vec_t)

/* not x1 and x2 */
MCVEC_FUN2(andnot1, vc, { MCVEC_INDENT(8)if(x2) MCVEC_INDENT(12)CALL(xor,vc,x1,true); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(set,c,false);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t, log_cst_t)

/* not x1 and x2 */
MCVEC_FUN2(andnot1, vv, { MCVEC_INDENT(8)CALL(andnot1, vv, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t, c_log_vec_t)

/* x1 and not x2 */
MCVEC_FUN2(andnot2, cv, { MCVEC_INDENT(8)FWD(andnot1,vc,x2,x1);MCVEC_INDENT(4)}, log_vec_t, log_cst_t, c_log_vec_t)

/* x1 and not x2 */
MCVEC_FUN2(andnot2, vc, { MCVEC_INDENT(8)FWD(andnot1,cv,x2,x1);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t, log_cst_t)

/* x1 and not x2 */
MCVEC_FUN2(andnot2, vv, { MCVEC_INDENT(8)CALL(andnot1,vv,x2,x1);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t, c_log_vec_t)

/* if x1 then x2+1 else x2 */
MCVEC_FUN2(condincr, cv, { MCVEC_INDENT(8)if(x1) MCVEC_INDENT(12)CALL(add,vc,x2,1.0); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(copy,v,x2);MCVEC_INDENT(4)}, num_vec_t, log_cst_t, c_num_vec_t)

/* if x1 then x2+1 else x2 */
MCVEC_FUN2(condincr, vc, { MCVEC_INDENT(8)CALL(condincr, vc, x1, x2);MCVEC_INDENT(4)}, num_vec_t, c_log_vec_t, num_cst_t)

/* if x1 then x2+1 else x2 */
MCVEC_FUN2(condincr, vv, { MCVEC_INDENT(8)CALL(condincr, vv, x1, x2);MCVEC_INDENT(4)}, num_vec_t, c_log_vec_t, c_num_vec_t)

/* copy */
MCVEC_FUN1(copy, v, { MCVEC_INDENT(8)CALL(copy, v, x1);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t)

/* division */
MCVEC_FUN2(div, cv, { MCVEC_INDENT(8)CALL(div, cv, x1, x2);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t)

/* division */
MCVEC_FUN2(div, vc, { MCVEC_INDENT(8)if(x2!=1.0) MCVEC_INDENT(12)CALL(div, vc, x1, x2); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(copy,v,x1);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t)

/* division */
MCVEC_FUN2(div, vv, { MCVEC_INDENT(8)CALL(div, vv, x1, x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t)

/* equal */
MCVEC_FUN2(eq, cv, { MCVEC_INDENT(8)CALL(eq,vc,x2,x1);MCVEC_INDENT(4)}, log_vec_t, num_cst_t, c_num_vec_t)

/* equal */
MCVEC_FUN2(eq, vc, { MCVEC_INDENT(8)CALL(eq, vc, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, num_cst_t)

/* equal */
MCVEC_FUN2(eq, vv, { MCVEC_INDENT(8)CALL(eq, vv, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, c_num_vec_t)

/* greater or equal */
MCVEC_FUN2(ge, cv, { MCVEC_INDENT(8)CALL(ge, cv, x1, x2);MCVEC_INDENT(4)}, log_vec_t, num_cst_t, c_num_vec_t)

/* greater or equal */
MCVEC_FUN2(ge, vc, { MCVEC_INDENT(8)CALL(ge, vc, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, num_cst_t)

/* greater or equal */
MCVEC_FUN2(ge, vv, { MCVEC_INDENT(8)CALL(ge, vv, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, c_num_vec_t)

/* x2 <= x1 <= x3 */
MCVEC_FUN3(gele, vcc, { MCVEC_INDENT(8)CALL(gele, vcc, x1, x2, x3);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, num_cst_t, num_cst_t)

/* x2 <= x1 <  x3 */
MCVEC_FUN3(gelt, vcc, { MCVEC_INDENT(8)CALL(gelt, vcc, x1, x2, x3);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, num_cst_t, num_cst_t)

/* greater */
MCVEC_FUN2(gt, cv, { MCVEC_INDENT(8)CALL(gt, cv, x1, x2);MCVEC_INDENT(4)}, log_vec_t, num_cst_t, c_num_vec_t)

/* greater */
MCVEC_FUN2(gt, vc, { MCVEC_INDENT(8)CALL(gt, vc, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, num_cst_t)

/* greater */
MCVEC_FUN2(gt, vv, { MCVEC_INDENT(8)CALL(gt, vv, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, c_num_vec_t)

/* x2 <  x1 <= x3 */
MCVEC_FUN3(gtle, vcc, { MCVEC_INDENT(8)CALL(gtle, vcc, x1, x2, x3);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, num_cst_t, num_cst_t)

/* x2 <  x1 <  x3 */
MCVEC_FUN3(gtlt, vcc, { MCVEC_INDENT(8)CALL(gtlt, vcc, x1, x2, x3);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, num_cst_t, num_cst_t)

/* if x1 then x2 * x3 else 0 */
MCVEC_FUN3(imult0, vcv, { MCVEC_INDENT(8)FWD(imult0, vvc, x1, x3, x2);MCVEC_INDENT(4)}, num_vec_t, c_log_vec_t, num_cst_t, c_num_vec_t)

/* if x1 then x2 * x3 else 0 */
MCVEC_FUN3(imult0, vvc, { MCVEC_INDENT(8)if(x3!=0.0) MCVEC_INDENT(12)if(x3!=1.0) MCVEC_INDENT(16)CALL(imult0, vvc, x1, x2, x3); MCVEC_INDENT(12)else MCVEC_INDENT(16)CALL(ite,vvc,x1,x2,0.0); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(set,c,0.0);MCVEC_INDENT(4)}, num_vec_t, c_log_vec_t, c_num_vec_t, num_cst_t)

/* if x1 then x2 * x3 else 0 */
MCVEC_FUN3(imult0, vvv, { MCVEC_INDENT(8)CALL(imult0, vvv, x1, x2, x3);MCVEC_INDENT(4)}, num_vec_t, c_log_vec_t, c_num_vec_t, c_num_vec_t)

/* indicator function: (cond? 1 : 0) */
MCVEC_FUN1(ind, v, { MCVEC_INDENT(8)CALL(and, vc, x1, 1.0);MCVEC_INDENT(4)}, num_vec_t, c_log_vec_t)

/* if x1 then x2 else x3 */
MCVEC_FUN3(ite, ccv, { MCVEC_INDENT(8)if(x1) MCVEC_INDENT(12)CALL(set,c,x2); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(copy,v,x3);MCVEC_INDENT(4)}, num_vec_t, log_cst_t, num_cst_t, c_num_vec_t)

/* if x1 then x2 else x3 */
MCVEC_FUN3(ite, cvc, { MCVEC_INDENT(8)if(x1) MCVEC_INDENT(12)CALL(copy,v,x2); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(set,c,x3);MCVEC_INDENT(4)}, num_vec_t, log_cst_t, c_num_vec_t, num_cst_t)

/* if x1 then x2 else x3 */
MCVEC_FUN3(ite, cvv, { MCVEC_INDENT(8)if(x1) MCVEC_INDENT(12)CALL(copy,v,x2); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(copy,v,x3);MCVEC_INDENT(4)}, num_vec_t, log_cst_t, c_num_vec_t, c_num_vec_t)

/* if x1 then x2 else x3 */
MCVEC_FUN3(ite, vcc, { MCVEC_INDENT(8)CALL(ite, vcc, x1, x2, x3);MCVEC_INDENT(4)}, num_vec_t, c_log_vec_t, num_cst_t, num_cst_t)

/* if x1 then x2 else x3 */
MCVEC_FUN3(ite, vcv, { MCVEC_INDENT(8)CALL(ite, vcv, x1, x2, x3);MCVEC_INDENT(4)}, num_vec_t, c_log_vec_t, num_cst_t, c_num_vec_t)

/* if x1 then x2 else x3 */
MCVEC_FUN3(ite, vvc, { MCVEC_INDENT(8)CALL(ite, vvc, x1, x2, x3);MCVEC_INDENT(4)}, num_vec_t, c_log_vec_t, c_num_vec_t, num_cst_t)

/* if x1 then x2 else x3 */
MCVEC_FUN3(ite, vvv, { MCVEC_INDENT(8)CALL(ite, vvv, x1, x2, x3);MCVEC_INDENT(4)}, num_vec_t, c_log_vec_t, c_num_vec_t, c_num_vec_t)

/* if x1 then x2 else 0 */
MCVEC_FUN2(itz, cv, { MCVEC_INDENT(8)if(x1) MCVEC_INDENT(12)CALL(copy,v,x2); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(set,c,0.0);MCVEC_INDENT(4)}, num_vec_t, log_cst_t, c_num_vec_t)

/* if x1 then x2 else 0 */
MCVEC_FUN2(itz, vc, { MCVEC_INDENT(8)CALL(and,vc,x1,x2);MCVEC_INDENT(4)}, num_vec_t, c_log_vec_t, num_cst_t)

/* if x1 then x2 else 0 */
MCVEC_FUN2(itz, vv, { MCVEC_INDENT(8)CALL(and,vv,x1,x2);MCVEC_INDENT(4)}, num_vec_t, c_log_vec_t, c_num_vec_t)

/* if x1 then 0 else x2 */
MCVEC_FUN2(ize, cv, { MCVEC_INDENT(8)if(x1) MCVEC_INDENT(12)CALL(set,c,0.0); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(copy,v,x2);MCVEC_INDENT(4)}, num_vec_t, log_cst_t, c_num_vec_t)

/* if x1 then 0 else x2 */
MCVEC_FUN2(ize, vc, { MCVEC_INDENT(8)CALL(andnot1,vc,x1,x2);MCVEC_INDENT(4)}, num_vec_t, c_log_vec_t, num_cst_t)

/* if x1 then 0 else x2 */
MCVEC_FUN2(ize, vv, { MCVEC_INDENT(8)CALL(andnot1,vv,x1,x2);MCVEC_INDENT(4)}, num_vec_t, c_log_vec_t, c_num_vec_t)

/* less or equal */
MCVEC_FUN2(le, cv, { MCVEC_INDENT(8)CALL(le, cv, x1, x2);MCVEC_INDENT(4)}, log_vec_t, num_cst_t, c_num_vec_t)

/* less or equal */
MCVEC_FUN2(le, vc, { MCVEC_INDENT(8)CALL(le, vc, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, num_cst_t)

/* less or equal */
MCVEC_FUN2(le, vv, { MCVEC_INDENT(8)CALL(le, vv, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, c_num_vec_t)

/* less */
MCVEC_FUN2(lt, cv, { MCVEC_INDENT(8)CALL(lt, cv, x1, x2);MCVEC_INDENT(4)}, log_vec_t, num_cst_t, c_num_vec_t)

/* less */
MCVEC_FUN2(lt, vc, { MCVEC_INDENT(8)CALL(lt, vc, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, num_cst_t)

/* less */
MCVEC_FUN2(lt, vv, { MCVEC_INDENT(8)CALL(lt, vv, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, c_num_vec_t)

/* max{x1 x2 x3} */
MCVEC_FUN3(max3, ccv, { MCVEC_INDENT(8)CALL(max, vc, x3, max(x1,x2));MCVEC_INDENT(4)}, num_vec_t, num_cst_t, num_cst_t, c_num_vec_t)

/* max{x1 x2 x3} */
MCVEC_FUN3(max3, cvc, { MCVEC_INDENT(8)CALL(max, vc, x2, max(x1,x3));MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t, num_cst_t)

/* max{x1 x2 x3} */
MCVEC_FUN3(max3, cvv, { MCVEC_INDENT(8)CALL(max3, vvc, x2, x3, x1);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t, c_num_vec_t)

/* max{x1 x2 x3} */
MCVEC_FUN3(max3, vcc, { MCVEC_INDENT(8)CALL(max, vc, x1, max(x2,x3));MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t, num_cst_t)

/* max{x1 x2 x3} */
MCVEC_FUN3(max3, vcv, { MCVEC_INDENT(8)CALL(max3, vvc, x1, x3, x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t, c_num_vec_t)

/* max{x1 x2 x3} */
MCVEC_FUN3(max3, vvc, { MCVEC_INDENT(8)CALL(max3, vvc, x1, x2, x3);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t, num_cst_t)

/* max{x1 x2 x3} */
MCVEC_FUN3(max3, vvv, { MCVEC_INDENT(8)CALL(max3, vvv, x1, x2, x3);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t, c_num_vec_t)

/* maximum */
MCVEC_FUN2(max, cv, { MCVEC_INDENT(8)CALL(max,vc,x2,x1);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t)

/* maximum */
MCVEC_FUN2(max, vc, { MCVEC_INDENT(8)CALL(max, vc, x1, x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t)

/* maximum */
MCVEC_FUN2(max, vv, { MCVEC_INDENT(8)CALL(max, vv, x1, x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t)

/* min{x1 x2 x3} */
MCVEC_FUN3(min3, ccv, { MCVEC_INDENT(8)CALL(min, vc, x3, min(x1,x2));MCVEC_INDENT(4)}, num_vec_t, num_cst_t, num_cst_t, c_num_vec_t)

/* min{x1 x2 x3} */
MCVEC_FUN3(min3, cvc, { MCVEC_INDENT(8)CALL(min, vc, x2, min(x1,x3));MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t, num_cst_t)

/* min{x1 x2 x3} */
MCVEC_FUN3(min3, cvv, { MCVEC_INDENT(8)CALL(min3, vvc, x2, x3, x1);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t, c_num_vec_t)

/* min{x1 x2 x3} */
MCVEC_FUN3(min3, vcc, { MCVEC_INDENT(8)CALL(min, vc, x1, min(x2,x3));MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t, num_cst_t)

/* min{x1 x2 x3} */
MCVEC_FUN3(min3, vcv, { MCVEC_INDENT(8)CALL(min3, vvc, x1, x3, x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t, c_num_vec_t)

/* min{x1 x2 x3} */
MCVEC_FUN3(min3, vvc, { MCVEC_INDENT(8)CALL(min3, vvc, x1, x2, x3);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t, num_cst_t)

/* min{x1 x2 x3} */
MCVEC_FUN3(min3, vvv, { MCVEC_INDENT(8)CALL(min3, vvv, x1, x2, x3);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t, c_num_vec_t)

/* minimum */
MCVEC_FUN2(min, cv, { MCVEC_INDENT(8)CALL(min,vc,x2,x1);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t)

/* minimum */
MCVEC_FUN2(min, vc, { MCVEC_INDENT(8)CALL(min, vc, x1, x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t)

/* minimum */
MCVEC_FUN2(min, vv, { MCVEC_INDENT(8)CALL(min, vv, x1, x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t)

/* x1 * x2 * x3 */
MCVEC_FUN3(mul3, ccv, { MCVEC_INDENT(8)FWD(mul, vc, x3, x1*x2);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, num_cst_t, c_num_vec_t)

/* x1 * x2 * x3 */
MCVEC_FUN3(mul3, cvc, { MCVEC_INDENT(8)FWD(mul, vc, x2, x1*x3);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t, num_cst_t)

/* x1 * x2 * x3 */
MCVEC_FUN3(mul3, cvv, { MCVEC_INDENT(8)FWD(mul3, vvc, x2, x3, x1);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t, c_num_vec_t)

/* x1 * x2 * x3 */
MCVEC_FUN3(mul3, vcc, { MCVEC_INDENT(8)FWD(mul, vc, x1, x2*x3);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t, num_cst_t)

/* x1 * x2 * x3 */
MCVEC_FUN3(mul3, vcv, { MCVEC_INDENT(8)FWD(mul3, vvc, x1, x3, x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t, c_num_vec_t)

/* x1 * x2 * x3 */
MCVEC_FUN3(mul3, vvc, { MCVEC_INDENT(8)if(x3!=0.0) MCVEC_INDENT(12)if(x3!=1.0) MCVEC_INDENT(16)CALL(mul3, vvc, x1, x2, x3); MCVEC_INDENT(12)else MCVEC_INDENT(16)CALL(mul,vv,x1,x2); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(set,c,0.0);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t, num_cst_t)

/* x1 * x2 * x3 */
MCVEC_FUN3(mul3, vvv, { MCVEC_INDENT(8)CALL(mul3, vvv, x1, x2, x3);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t, c_num_vec_t)

/* multiplication */
MCVEC_FUN2(mul, cv, { MCVEC_INDENT(8)FWD(mul,vc,x2,x1);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t)

/* multiplication */
MCVEC_FUN2(mul, vc, { MCVEC_INDENT(8)if(x2!=0.0) MCVEC_INDENT(12)if(x2!=1) MCVEC_INDENT(16)CALL(mul, vc, x1, x2); MCVEC_INDENT(12)else MCVEC_INDENT(16)CALL(copy,v,x1); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(set,c,0.0);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t)

/* multiplication */
MCVEC_FUN2(mul, vv, { MCVEC_INDENT(8)CALL(mul, vv, x1, x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t)

/* x1 * x2 + x3 */
MCVEC_FUN3(muladd, ccv, { MCVEC_INDENT(8)FWD(add, vc, x3, x1*x2);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, num_cst_t, c_num_vec_t)

/* x1 * x2 + x3 */
MCVEC_FUN3(muladd, cvc, { MCVEC_INDENT(8)FWD(muladd, vcc, x2, x1, x3);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t, num_cst_t)

/* x1 * x2 + x3 */
MCVEC_FUN3(muladd, cvv, { MCVEC_INDENT(8)FWD(muladd,vcv,x2,x1,x3);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t, c_num_vec_t)

/* x1 * x2 + x3 */
MCVEC_FUN3(muladd, vcc, { MCVEC_INDENT(8)if(x3!=0.0) MCVEC_INDENT(12)if(x2!=0) MCVEC_INDENT(16)if(x2!=1.0) MCVEC_INDENT(20)CALL(muladd, vcc, x1, x2, x3); MCVEC_INDENT(16)else MCVEC_INDENT(20)CALL(add,vc,x1,x3); MCVEC_INDENT(12)else MCVEC_INDENT(16)CALL(set,c,x3); MCVEC_INDENT(8)else MCVEC_INDENT(12)FWD(mul,vc,x1,x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t, num_cst_t)

/* x1 * x2 + x3 */
MCVEC_FUN3(muladd, vcv, { MCVEC_INDENT(8)if(x2!=0.0) MCVEC_INDENT(12)if(x2!=1.0) MCVEC_INDENT(16)CALL(muladd, vcv, x1, x2, x3); MCVEC_INDENT(12)else MCVEC_INDENT(16)CALL(add,vv,x1,x3); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(copy,v,x3);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t, c_num_vec_t)

/* x1 * x2 + x3 */
MCVEC_FUN3(muladd, vvc, { MCVEC_INDENT(8)if(x3!=0.0) MCVEC_INDENT(12)CALL(muladd, vvc, x1, x2, x3); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(mul,vv,x1,x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t, num_cst_t)

/* x1 * x2 + x3 */
MCVEC_FUN3(muladd, vvv, { MCVEC_INDENT(8)CALL(muladd, vvv, x1, x2, x3);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t, c_num_vec_t)

/* if x1!=0 then x1*x2 else 0 */
MCVEC_FUN2(mulnz, cv, { MCVEC_INDENT(8)if(x1!=0.0) MCVEC_INDENT(12)CALL(mul,vc,x2,x1); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(set,c,0.0);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t)

/* if x1!=0 then x1*x2 else 0 */
MCVEC_FUN2(mulnz, vc, { MCVEC_INDENT(8)CALL(mulnz, vc, x1, x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t)

/* if x1!=0 then x1*x2 else 0 */
MCVEC_FUN2(mulnz, vv, { MCVEC_INDENT(8)CALL(mulnz, vv, x1, x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t)

/* x1 * x2 - x3 */
MCVEC_FUN3(mulsub, ccv, { MCVEC_INDENT(8)FWD(sub, cv, x1*x2, x3);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, num_cst_t, c_num_vec_t)

/* x1 * x2 - x3 */
MCVEC_FUN3(mulsub, cvc, { MCVEC_INDENT(8)FWD(mulsub, vcc, x2, x1, x3);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t, num_cst_t)

/* x1 * x2 - x3 */
MCVEC_FUN3(mulsub, cvv, { MCVEC_INDENT(8)FWD(mulsub, vcv, x2, x1, x3);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t, c_num_vec_t)

/* x1 * x2 - x3 */
MCVEC_FUN3(mulsub, vcc, { MCVEC_INDENT(8)if(x3!=0.0) MCVEC_INDENT(12)if(x2!=0) MCVEC_INDENT(16)if(x2!=1.0) MCVEC_INDENT(20)CALL(mulsub, vcc, x1, x2, x3); MCVEC_INDENT(16)else MCVEC_INDENT(20)CALL(sub,vc,x1,x3); MCVEC_INDENT(12)else MCVEC_INDENT(16)CALL(set,c,-x3); MCVEC_INDENT(8)else MCVEC_INDENT(12)FWD(mul,vc,x1,x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t, num_cst_t)

/* x1 * x2 - x3 */
MCVEC_FUN3(mulsub, vcv, { MCVEC_INDENT(8)if(x2!=0.0) MCVEC_INDENT(12)if(x2!=1.0) MCVEC_INDENT(16)CALL(mulsub, vcv, x1, x2, x3); MCVEC_INDENT(12)else MCVEC_INDENT(16)CALL(sub,vv,x1,x3); MCVEC_INDENT(8)else MCVEC_INDENT(12)FWD(neg,v,x3);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t, c_num_vec_t)

/* x1 * x2 - x3 */
MCVEC_FUN3(mulsub, vvc, { MCVEC_INDENT(8)if(x3!=0.0) MCVEC_INDENT(12)CALL(mulsub, vvc, x1, x2, x3); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(mul,vv,x1,x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t, num_cst_t)

/* x1 * x2 + x3 */
MCVEC_FUN3(mulsub, vvv, { MCVEC_INDENT(8)CALL(mulsub, vvv, x1, x2, x3);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t, c_num_vec_t)

/* logical and */
MCVEC_FUN2(nand, cv, { MCVEC_INDENT(8)FWD(nand,vc,x2,x1);MCVEC_INDENT(4)}, log_vec_t, log_cst_t, c_log_vec_t)

/* logical and */
MCVEC_FUN2(nand, vc, { MCVEC_INDENT(8)if(x2) MCVEC_INDENT(12)CALL(andnot1,vc,x1,true); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(set,c,true);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t, log_cst_t)

/* logical and */
MCVEC_FUN2(nand, vv, { MCVEC_INDENT(8)CALL(nand, vv, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t, c_log_vec_t)

/* not equal */
MCVEC_FUN2(ne, cv, { MCVEC_INDENT(8)CALL(ne,vc,x2,x1);MCVEC_INDENT(4)}, log_vec_t, num_cst_t, c_num_vec_t)

/* not equal */
MCVEC_FUN2(ne, vc, { MCVEC_INDENT(8)CALL(ne, vc, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, num_cst_t)

/* not equal */
MCVEC_FUN2(ne, vv, { MCVEC_INDENT(8)CALL(ne, vv, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, c_num_vec_t)

/* change sign */
MCVEC_FUN1(neg, v, { MCVEC_INDENT(8)CALL(xor, vc, x1, i2d(0x8000000000000000));MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t)

/* not (x2 <= x1 <= x3) */
MCVEC_FUN3(ngele, vcc, { MCVEC_INDENT(8)CALL(ngele, vcc, x1, x2, x3);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, num_cst_t, num_cst_t)

/* not (x2 <= x1 <  x3) */
MCVEC_FUN3(ngelt, vcc, { MCVEC_INDENT(8)CALL(ngelt, vcc, x1, x2, x3);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, num_cst_t, num_cst_t)

/* not (x2 <  x1 <= x3) */
MCVEC_FUN3(ngtle, vcc, { MCVEC_INDENT(8)CALL(ngtle, vcc, x1, x2, x3);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, num_cst_t, num_cst_t)

/* not (x2 <  x1 <  x3) */
MCVEC_FUN3(ngtlt, vcc, { MCVEC_INDENT(8)CALL(ngtlt, vcc, x1, x2, x3);MCVEC_INDENT(4)}, log_vec_t, c_num_vec_t, num_cst_t, num_cst_t)

/* logical and */
MCVEC_FUN2(nor, cv, { MCVEC_INDENT(8)FWD(nor,vc,x2,x1);MCVEC_INDENT(4)}, log_vec_t, log_cst_t, c_log_vec_t)

/* logical and */
MCVEC_FUN2(nor, vc, { MCVEC_INDENT(8)if(!x2) MCVEC_INDENT(12)CALL(andnot1,vc,x1,true); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(set,c,false);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t, log_cst_t)

/* logical and */
MCVEC_FUN2(nor, vv, { MCVEC_INDENT(8)CALL(nor, vv, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t, c_log_vec_t)

/* negate */
MCVEC_FUN1(not, v, { MCVEC_INDENT(8)CALL(xor, vc, x1, true);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t)

/* logical xor */
MCVEC_FUN2(nxor, cv, { MCVEC_INDENT(8)FWD(nxor,vc,x2,x1);MCVEC_INDENT(4)}, log_vec_t, log_cst_t, c_log_vec_t)

/* logical xor */
MCVEC_FUN2(nxor, vc, { MCVEC_INDENT(8)if(!x2) MCVEC_INDENT(12)CALL(andnot1,vc,x1,true); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(copy,v,x1);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t, log_cst_t)

/* logical xor */
MCVEC_FUN2(nxor, vv, { MCVEC_INDENT(8)CALL(nxor, vv, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t, c_log_vec_t)

/* logical or */
MCVEC_FUN2(or, cv, { MCVEC_INDENT(8)FWD(or,vc,x2,x1);MCVEC_INDENT(4)}, log_vec_t, log_cst_t, c_log_vec_t)

/* logical or */
MCVEC_FUN2(or, vc, { MCVEC_INDENT(8)if(x2) MCVEC_INDENT(12)CALL(set,c,true); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(copy,v,x1);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t, log_cst_t)

/* logical or */
MCVEC_FUN2(or, vv, { MCVEC_INDENT(8)CALL(or, vv, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t, c_log_vec_t)

/* not x1 or x2 */
MCVEC_FUN2(ornot1, cv, { MCVEC_INDENT(8)if(x1) MCVEC_INDENT(12)CALL(copy,v,x2); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(set,c,true);MCVEC_INDENT(4)}, log_vec_t, log_cst_t, c_log_vec_t)

/* not x1 or x2 */
MCVEC_FUN2(ornot1, vc, { MCVEC_INDENT(8)if(!x2) MCVEC_INDENT(12)CALL(andnot1,vc,x1,true); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(set,c,true);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t, log_cst_t)

/* not x1 or x2 */
MCVEC_FUN2(ornot1, vv, { MCVEC_INDENT(8)CALL(ornot1, vv, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t, c_log_vec_t)

/* assign */
MCVEC_FUN1(set, c, { MCVEC_INDENT(8)CALL(set, c, x1);MCVEC_INDENT(4)}, num_vec_t, num_cst_t)

/* square root */
MCVEC_FUN1(sqrt, v, { MCVEC_INDENT(8)CALL(sqrt, v, x1);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t)

/* subtraction */
MCVEC_FUN2(sub, cv, { MCVEC_INDENT(8)if(x1!=0.0) MCVEC_INDENT(12)CALL(sub, cv, x1, x2); MCVEC_INDENT(8)else MCVEC_INDENT(12)FWD(neg,v,x2);MCVEC_INDENT(4)}, num_vec_t, num_cst_t, c_num_vec_t)

/* subtraction */
MCVEC_FUN2(sub, vc, { MCVEC_INDENT(8)if(x2!=0.0) MCVEC_INDENT(12)CALL(sub, vc, x1, x2); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(copy,v,x1);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, num_cst_t)

/* subtraction */
MCVEC_FUN2(sub, vv, { MCVEC_INDENT(8)CALL(sub, vv, x1, x2);MCVEC_INDENT(4)}, num_vec_t, c_num_vec_t, c_num_vec_t)

/* logical xor */
MCVEC_FUN2(xor, cv, { MCVEC_INDENT(8)FWD(xor,vc,x2,x1);MCVEC_INDENT(4)}, log_vec_t, log_cst_t, c_log_vec_t)

/* logical xor */
MCVEC_FUN2(xor, vc, { MCVEC_INDENT(8)if(x2) MCVEC_INDENT(12)CALL(xor, vc, x1, x2); MCVEC_INDENT(8)else MCVEC_INDENT(12)CALL(copy,v,x1);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t, log_cst_t)

/* logical xor */
MCVEC_FUN2(xor, vv, { MCVEC_INDENT(8)CALL(xor, vv, x1, x2);MCVEC_INDENT(4)}, log_vec_t, c_log_vec_t, c_log_vec_t)
#undef MCVEC_FUN1
#undef MCVEC_FUN2
#undef MCVEC_FUN3
#ifdef MCVEC_FUN
#    undef MCVEC_FUN
#endif
