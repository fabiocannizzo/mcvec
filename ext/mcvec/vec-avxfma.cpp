#if !defined(__AVX__) || !defined(__FMA__) || defined(__AVX2__)
#error This file must be compiled with avx and fma instructions
#endif

#include "vec-xxx.h"

template struct OuterPtrSet<4, true, true>; // force instantiation
