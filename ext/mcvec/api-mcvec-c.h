#pragma once

#ifndef MCVEC_DLL
#define MCVEC_DLL
#endif

#include "typedef.h"

extern "C"
{

MCVEC_DLL const char *mcvec_instr_set();

#include "gen-head.h"

} // extern "C"
