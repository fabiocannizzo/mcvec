#include "vec-names.h"
#define MCVEC_HEAD(nm,m,rt,...) void MCVEC_FUNNAME(nm,m)(vec_size_t n, rt dst, __VA_ARGS__)
#define MCVEC_BODY(nm,m,rt,...) { Dispatcher<MCVEC_ENUMNAME(nm,m)>::run(n, dst, __VA_ARGS__); }
#define MCVEC_FUN1(n,m,body,rt,at) MCVEC_HEAD(n,m,rt,at x1) MCVEC_BODY(n,m,rt,x1)
#define MCVEC_FUN2(n,m,body,rt,at,bt) MCVEC_HEAD(n,m,rt,at x1, bt x2) MCVEC_BODY(n,m,rt,x1, x2)
#define MCVEC_FUN3(n,m,body,rt,at,bt,ct) MCVEC_HEAD(n,m,rt,at x1, bt x2, ct x3) MCVEC_BODY(n,m,rt,x1, x2, x3)

#include "auto/all-fun.h"

#undef MCVEC_HEAD
#undef MCVEC_BODY


