#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cctype>
#include <algorithm>
#include <functional>
#include <vector>
#include <set>
#include <cassert>
#include <regex>
#include <iterator>

using namespace std;

const vector<string> args( { "num_vec_t", "num_cst_t", "c_num_vec_t", "log_vec_t", "log_cst_t", "c_log_vec_t" } );

const string notice = "// This is an auto generated file\n\n";

bool my_isblank(char ch)
{
    return std::isblank(static_cast<unsigned char>(ch)) != 0;
}

bool isarg(const string& s)
{
    return find(args.begin(), args.end(), s) != args.end();
}

bool isscalar(const string& s)
{
    return s == "num_cst_t" || s == "log_cst_t";
}

char getmode(const string& s)
{
    return (s == "c_num_vec_t" || s == "c_log_vec_t") ? 'v' : 'c';
}

string mkinpfn(const string& grp)
{
    return "x-" + grp + "-codes.csv";
}

bool validline(const string& ln)
{
    for (char c : ln)
        if (!my_isblank(c))
            return c != '#';
    return false;
}

void removeblank(string& s)
{
    auto notblank = [](char c){ return !my_isblank(c); };
    auto i = distance(s.begin(), find_if(s.begin(), s.end(), notblank));
    auto e = distance(s.rbegin(), find_if(s.rbegin(), s.rend(), notblank));
    s = s.substr(i, s.length() - i - e);
}

istringstream& gettoken(istringstream& is, string& s)
{
    getline(is, s, ',');
    removeblank(s);
    return is;
}


struct Fun
{
    Fun(const string& grp, const string& ln) : grp(grp), used(false)
    {
        istringstream is(ln);

        gettoken(is, name);

        string token;
        while(1) {
            gettoken(is, token);
            if (isarg(token))
                args.push_back(token);
            else
                break;
        }

        notes = "/* " + token + " */";

        for (size_t i = 0; i < nargs(); ++i)
            mode += getmode(args[i]);
        key = name + "_" + mode;
        fname = "MCVEC_FUNNAME(" + name + "," + mode  +")";
        mangle = "_" + name;
        id = "id" + mangle;

        getline(is, dispatch, char(0)); // all the rest is part of dispatching
        removeblank(dispatch);
        size_t i;
        while((i = dispatch.find("DEF")) < string::npos)
            dispatch.replace(i, 3, def());
    }

    bool operator<(const Fun& f) { return key < f.key; }
    bool operator==(const Fun& f) { return key == f.key; }

    size_t nargs() const { return args.size() - 1; }

    string bodyline(const vector<string>& vals) const
    {
        ostringstream os;
        const size_t n = nargs();
        assert(n == vals.size());
        os << "CALL" << "(" << name << ", " << mode << ", " << vals[0];
        for (size_t i = 1; i < n; ++i)
            os << ", " << vals[i];
        os << ")";
        used = true;
        return os.str();
    }

    string mkbody() const
    {
        ostringstream os;
        os << "{";
        istringstream is(dispatch);
        string line;
        while(getline(is, line, '\\')) {
            auto n = line.find_first_not_of(" ");
            if(n != string::npos) {
                os << " MCVEC_INDENT(" << 8+n*4 << ")"
                    << line.substr(n);
            }
            else
                os << line;
        }
        os << "MCVEC_INDENT(4)}";
       
        return os.str();
    }

    string def() const
    {
        vector<string> args;
        for (size_t i = 0; i < nargs(); ++i)
            args.push_back(string("x") + char('1' + i));
        return bodyline(args);
    }

    string key;
    string fname;
    string id;
    string name;
    string mode;
    string mangle;
    vector<string> args;
    string notes;
    string dispatch;
    string grp;
    mutable bool used;
};

vector<Fun> funs;

const Fun& findkey(const string& key)
{
    auto f = find_if(funs.cbegin(), funs.cend(), [&key](auto& f)->bool {return f.key == key; });
    if (f == funs.cend()) {
        cout << key << " not found\n";
        exit(-1);
    }
    return *f;
}


void parsefile(const string& grp)
{
    string fn = mkinpfn(grp);
    cout << "processing file: " << fn << endl;
    ifstream is(fn);
    assert(is);
   // cout << "opened file " << fn << endl;
    string ln;
    while (getline(is, ln)) {
        //cout << "line:\n" << ln << endl;
        if(validline(ln)) {
            funs.push_back(Fun(grp, ln));
        //    cout << "...used\n";
        }
        //else
        //    cout << "...skipped\n";
    }
}


void mkenumfile(const string& grp)
{
    string fn = "auto/" + grp + "-enum.h";
    ofstream os(fn);
    os << notice;
    set<string> ids;
    for (const Fun& f : funs)
        if (f.grp == grp && ids.insert(f.id).second)
            os << f.id << ",\n";
}

void mkmodefile()
{
    string fn = "auto/all-modes.h";
    ofstream os(fn);
    os << notice;
    set<string> ids;
    for (const Fun& f : funs)
        if (ids.insert(f.mode).second)
            os << f.mode << ",\n";
}

void undef(ostream& os)
{
    os << "#undef MCVEC_FUN1\n";
    os << "#undef MCVEC_FUN2\n";
    os << "#undef MCVEC_FUN3\n";
    os << "#ifdef MCVEC_FUN\n";
    os << "#    undef MCVEC_FUN\n";
    os << "#endif\n";
}

void def(ostream& os)
{
    auto check = [&](auto i) {
            os << "#ifndef MCVEC_FUN" << i << "\n";
            os << "#    error MCVEC_FUN" << i << " is not defined\n";
            os << "#endif\n";
        };
    check(1);
    check(2);
    check(3);
}

void mkallfunfile()
{
    ofstream os("auto/all-fun.h");
    os << notice;
    def(os);
    os << "#ifndef MCVEC_INDENT\n";
    os << "#    define MCVEC_INDENT(n)\n";
    os << "#endif\n";
    for (const Fun& f : funs) {
        os << "\n"
           << f.notes << endl
           << "MCVEC_FUN" << f.nargs() << "(" << f.name << ", " << f.mode << ", " << f.mkbody() << ", " << f.args.back();
        for (size_t i = 1; i <= f.nargs(); ++i)
             os << ", " << f.args[i-1];
        os << ")\n";
    }

    undef(os);
}

void mkallloopfile()
{
    ofstream os("auto/all-loop.h");
    os << notice;
    def(os);
    for (const Fun& f : funs) {
        if(f.used) {
            os << "MCVEC_FUN" << f.nargs() << "(" << f.name << ", " << f.mode << ", " << f.args.back();
            for(size_t i = 1; i <= f.nargs(); ++i)
                os << ", " << f.args[i - 1];
            os << ")\n";
        }
    }
    undef(os);
}


void identifyinuse()
{
    const string r("CALL[ ]*[(][ ]*([a-z0-9]+)[ ]*,[ ]*([vc]+)");
    regex words_regex(r);

    for (const Fun& f : funs) {
        const string& s = f.dispatch;
        auto words_begin = sregex_iterator(s.begin(), s.end(), words_regex);
        auto words_end = sregex_iterator();

        for (sregex_iterator i = words_begin; i != words_end; ++i) {
            smatch match = *i;
            string key = match[1].str() + "_" + match[2].str();
            findkey(key).used = true;
        }
    }
}

int main(int argc, const char **argv)
{
    for (int i = 1; i < argc; ++i)
        parsefile(argv[i]);
    sort(funs.begin(), funs.end());
    auto iter = adjacent_find(funs.begin(), funs.end());
    if (iter != funs.end()) {
        cout << "duplicate function " << iter->key << "\n";
        return -1;
    }
    cout << "parsed " << funs.size() << " functions\n";

    identifyinuse();
    for (int i = 1; i < argc; ++i)
        mkenumfile(argv[i]);
    mkmodefile();
    mkallfunfile();
    mkallloopfile();
    return 0;
}

