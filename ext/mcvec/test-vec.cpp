#include "api-mcvec-vec.h"
#include "vec-utils.h"

#include <cstdint>
#include <cstdio>
#include <functional>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <limits>
#include <array>
#include <vector>
#include <numeric>

using namespace std;
using namespace mcvec;

const unsigned offset_end = 8;  // offset gors from 0 to (offset_end-1)
const size_t n = 1024 + 4 + 2 + 1;  // trigger all possible cases in the splitter

const unsigned sz = n + (offset_end - 1);
const unsigned slack = (8 - (sz % 8)) % 8;
const unsigned sn = sz + slack;
alignas(64) double arg_vecs[3][2][sn], ret_vec[sn];

void init_args()
{
    srand(1);
    auto to_logical = [](double x) { return b2d(x > 10); };
    for(int i = 0; i < sn; ++i) {
        arg_vecs[0][0][i] = rand() % 20;
        arg_vecs[1][0][i] = 1 + rand() % 20;
        arg_vecs[2][0][i] = rand() % 20;
        for(int j = 0; j < sizeof(arg_vecs) / sizeof(arg_vecs[0]); ++j)
            arg_vecs[j][1][i] = to_logical(arg_vecs[j][0][i]);
    }
}

template <template <typename> class PRED>
double cmp(double a, double b) { return b2d(PRED<double>()(a, b)); }

template <template <typename> class PRED>
double icmp(double a, double b) { return i2d(PRED<int64_t>()(d2i(a), d2i(b))); }

template <code C>
void not_implemented()
{
    cout << "operator not implemented: " << C << "\n";
    exit(1);
}

template <code C>
double testfun(double x)
{
    switch(C) {
        case id_set: return x;
        case id_copy: return x;
        case id_neg: return -x;
        case id_not: return i2d(~d2i(x));
        case id_abs: return std::abs(x);
        case id_sqrt: return std::sqrt(x);
        case id_ind: return x != 0.0 ? 1.0 : 0.0;
        default: not_implemented<C>();
    }
}

template <code C>
double testfun(double x, double y)
{
    switch(C) {
        case id_mulnz: return x != 0.0 ? x * y : 0.0;

        case id_add: return x + y;
        case id_sub: return x - y;
        case id_mul: return x * y;
        case id_div: return x / y;
        case id_min: return min(x, y);
        case id_max: return max(x, y);

        case id_and: return icmp<bit_and>(x, y);
        case id_andnot1: return i2d(bit_and<int64_t>()(~d2i(x), d2i(y)));
        case id_ornot1: return i2d(bit_or<int64_t>()(~d2i(x), d2i(y)));
        case id_andnot2: return i2d(bit_and<int64_t>()(d2i(x), ~d2i(y)));
        case id_or: return icmp<bit_or>(x, y);
        case id_xor: return icmp<bit_xor>(x, y);
        case id_nand: return i2d(~d2i(icmp<bit_and>(x, y)));
        case id_nor: return i2d(~d2i(icmp<bit_or>(x, y)));
        case id_nxor: return i2d(~d2i(icmp<bit_xor>(x, y)));

        case id_ge: return cmp<greater_equal>(x, y);
        case id_gt: return cmp<greater>(x, y);
        case id_le: return cmp<less_equal>(x, y);
        case id_lt: return cmp<less>(x, y);
        case id_eq: return cmp<equal_to>(x, y);
        case id_ne: return cmp<not_equal_to>(x, y);

        case id_itz: return d2i(x) ? y : 0.0;
        case id_ize: return d2i(x) ? 0.0 : y;
        case id_condincr: return d2i(x) ? y + 1.0 : y;
        default: not_implemented<C>();
    }
}

template <code C>
double testfun(double x, double y, double z)
{
    switch(C) {
        case id_add3: return x + y + z;
        case id_mul3: return x * y * z;
        case id_min3: return min(min(x, y), z);
        case id_max3: return max(max(x, y), z);

        case id_imult0: return icmp<bit_and>(x, y * z);

        case id_gele: return b2d(x >= y && x <= z);
        case id_gelt: return b2d(x >= y && x <  z);
        case id_gtle: return b2d(x >  y && x <= z);
        case id_gtlt: return b2d(x >  y && x <  z);
        case id_ngele: return b2d(!(x >= y && x <= z));
        case id_ngelt: return b2d(!(x >= y && x <  z));
        case id_ngtle: return b2d(!(x >  y && x <= z));
        case id_ngtlt: return b2d(!(x >  y && x <  z));

        case id_ite: return d2i(x) ? y : z;
        case id_muladd: return x * y + z;
        case id_mulsub: return x * y - z;
        default: not_implemented<C>();
    }
}

double get(bool f, vec_size_t i) { return b2d(f); }
double get(double v, vec_size_t i) { return v; }
double get(const double *p, vec_size_t i) { return p[i]; }

template <typename T> struct Cvt { double operator()(double x) { return x; } };
template <> struct Cvt<log_vec_t> { int64_t operator()(double x) { return d2i(x); } };
template <> struct Cvt<bool> { int64_t operator()(double x) { return d2i(x); } };
template <> struct Cvt<c_log_vec_t> { int64_t operator()(double x) { return d2i(x); } };

template <typename T> constexpr unsigned max_ptr_offset() { return offset_end; };
template <> constexpr unsigned max_ptr_offset<bool>() { return 1; };
template <> constexpr unsigned max_ptr_offset<double>() { return 1; };

template <typename T> T apply_offset(unsigned offset, T p) { return p + offset; }
template <> bool apply_offset(unsigned, bool v) { return v; }
template <> double apply_offset(unsigned, double v) { return v; }

template <typename O, unsigned Level> struct ArgTypeImpl;
template <typename O> struct ArgTypeImpl<O, 0> { typedef typename O::arg1_t type_t; };
template <typename O> struct ArgTypeImpl<O, 1> { typedef typename O::arg2_t type_t; };
template <typename O> struct ArgTypeImpl<O, 2> { typedef typename O::arg3_t type_t; };
template <code C, mode M, unsigned Level>
using ArgType = typename ArgTypeImpl<Op<C, M>, Level>::type_t;

void printValue(const char *msg, double v)
{
    cout << setw(15) << msg << ": "
         << dec << scientific << setprecision(15) << v << " (" << hex << d2i(v) << ")\n";
}
void printValue(const char *msg, int64_t v)
{
    cout << setw(15) << msg << ": " << hex << v << "\n";
}

template <code id, mode m, typename...Args, size_t...Is>
void printArgs(index_sequence<Is...>&&, Args...xs)
{
    char buffer[32];
    auto makeArgString = [&buffer](size_t n) -> const char *
        {
            sprintf(buffer, "arg%lu", n);
            return buffer;
        };

    (..., printValue(makeArgString(Is), Cvt<ArgType<id, m, Is>>()(xs)));
}

template <code id, mode m, typename...Args>
void test3(unsigned *offset, double *ores, Args...args)
{
    for (vec_size_t i = 0; i < n; ++i)
        ores[i] = numeric_limits<double>::quiet_NaN();
    Op<id,m>::dispatch(n, ores, args...);

    Cvt<typename Op<id,m>::ret_t> cvtfun;
    for (vec_size_t i = 0; i < n; ++i) {
        constexpr size_t nargs = sizeof...(Args);
        auto dc = testfun<id>(get(args, i)...);
        if (cvtfun(ores[i]) != cvtfun(dc) && d2i(ores[i]) != d2i(dc)) {
            cout << "\nerror at element " << i << "\n";
            cout << "\toffsets: ";
            for(unsigned i = 0; i < 1 + nargs; ++i)
                cout << offset[i];
            cout << "\n";
            printValue("expected", cvtfun(dc));
            printValue("got", cvtfun(ores[i]));
            printArgs<id, m>(make_index_sequence<nargs>{}, get(args, i)...);
            exit(1);
        }
    }
}

template <code id, mode m, unsigned...Is, typename...Args>
void test2(unsigned *offset, integer_sequence<unsigned, Is...>, Args...args)
{
    test3<id, m>(offset, apply_offset(offset[Is], args)...);
}

template <code id, mode m, typename...Args>
void test1(unsigned level, unsigned *offset, const unsigned *offset_end, Args...args)
{
    if(level == sizeof...(Args)) {
        test2<id, m>(offset, make_integer_sequence<unsigned, sizeof...(Args)>{}, args...);
    }
    else {
        for(unsigned i = 0; i < offset_end[level]; ++i) {
            offset[level] = i;
            test1<id, m>(level+1, offset, offset_end, args...);
        }
    }
}

template <code id, mode m, typename...Args>
void test0(Args...args)
{
    unsigned offset[sizeof...(Args)] = {0};
    unsigned offset_end[sizeof...(Args)] = {max_ptr_offset<Args>()...};
    test1<id, m>(0, offset, offset_end, args...);
}


template <class T, size_t = sizeof(T)>
true_type is_complete_impl(T *);

false_type is_complete_impl(void *);

template <class T>
using is_complete = decltype(is_complete_impl(declval<T*>()));

// run test if the type is complete, i.e. if the combination code-mode is defined
template <code C, mode M>
void run_test(false_type&&) { /* do nothing */ }

template <code C, typename Arg, unsigned Level> struct PossibleArgValues;
template <code C, unsigned Level> struct PossibleArgValues<C, bool, Level> { static constexpr bool values[] = { false, true }; };
template <code C, unsigned Level> struct PossibleArgValues<C, double, Level> { static constexpr double values[] = { 0.0, 1.0, 10.0 }; };
template <code C, unsigned Level> struct PossibleArgValues<C, c_num_vec_t, Level> { static constexpr const double * values[] = { arg_vecs[Level][0] }; };
template <code C, unsigned Level> struct PossibleArgValues<C, c_log_vec_t, Level> { static constexpr const double * values[] = {arg_vecs[Level][1] }; };
template <> struct PossibleArgValues<id_div, double, 2> { static constexpr double values[] = {1.0, 10.0}; };

template <code C, mode M, unsigned Level>
using ArgValues = PossibleArgValues<C, ArgType<C,M,Level>, Level>;

template <code C, mode M, unsigned Level>
constexpr size_t ArgValuesSize() { return sizeof(ArgValues<C,M,Level>::values) / sizeof(ArgType<C, M, Level>); }

template <code C, mode M, typename...Args>
void run_test_impl(true_type&& complete, double *r, Args...args)
{
    test0<C, M>(r, args...);
}

template <code C, mode M, typename...Args>
void run_test_impl(false_type&& complete, double *r, Args...args);

template <code C, mode M, size_t...As, typename...Args>
void run_test_impl0(index_sequence<As...>, double *r, Args...args)
{
    constexpr unsigned Level = sizeof...(Args);
    typedef ArgValues<C, M, Level> possible_t;
    using complete = bool_constant<Op<C,M>::nargs == 1 + Level>;
    (... , run_test_impl<C, M>(complete{}, r, args..., possible_t::values[As]));
}

template <code C, mode M, typename...Args>
void run_test_impl(false_type&& complete, double *r, Args...args)
{
    constexpr size_t nvalues = ArgValuesSize<C, M, sizeof...(Args)>();
    run_test_impl0<C, M>(make_index_sequence<nvalues>{}, r, args...);
}

template <code C, mode M>
void run_test(true_type&&)
{
    cout << "testing : " << Op<C, M>::name() << " ... ";
    cout.flush();
    run_test_impl<C, M>(false_type{}, ret_vec);
    cout << "done" << endl;
}

// transform the mode integer sequence in an argument pack
// invoke run_test passing either true_type of false_type
template <code C, int...Ms>
void run_tests1(integer_sequence<int, Ms...>)
{
    (..., run_test<C, static_cast<mode>(Ms)>(is_complete<Op<C, static_cast<mode>(Ms)>>{}));
}

// transform the code integer sequence in an argument pack
template <int...Cs>
void run_tests0(integer_sequence<int, Cs...>)
{
    (... , run_tests1<static_cast<code>(Cs)>(make_integer_sequence<int, mode_end>{}));
}

int main()
{
    cout << "Instruction set selected: " << mcvec_instr_set() << "\n";
    init_args();
    run_tests0(make_integer_sequence<int, code_end>{});
    return 0;
}
