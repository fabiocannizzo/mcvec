#include "vec-names.h"
#define MCVEC_FUN(n,m,...) \
template <>\
struct LoopPtrTraits<MCVEC_ENUMNAME(n,m)> {\
   typedef void (*type_t)(vec_size_t, double *, __VA_ARGS__);\
};

#define num_vec_t vector_arg_t
#define c_num_vec_t vector_arg_t
#define log_vec_t vector_arg_t
#define c_log_vec_t vector_arg_t
#define num_cst_t scalar_arg_t
#define log_cst_t scalar_arg_t

#define MCVEC_FUN1(n,m,rt,...) MCVEC_FUN(n,m,__VA_ARGS__)
#define MCVEC_FUN2(n,m,rt,...) MCVEC_FUN(n,m,__VA_ARGS__)
#define MCVEC_FUN3(n,m,rt,...) MCVEC_FUN(n,m,__VA_ARGS__)

#include "auto/all-loop.h"

#undef num_vec_t
#undef c_num_vec_t
#undef log_vec_t
#undef c_log_vec_t
#undef num_cst_t
#undef log_cst_t
