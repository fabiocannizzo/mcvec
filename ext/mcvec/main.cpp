#include "cpu.h"
#include "export.h"

#include <cassert>

enum InstrSet { SSE, AVX, AVXFMA, AVX512FMA };
static const char *isnames[]= { "SSE4.1", "AVX", "AVX+FMA", "AVX512DQ+FMA" };

struct SelectInstrSet
{
    SelectInstrSet()
    {
        CPUFeatures cpu;

#if (SKIP_AVX512 != 1) && !defined(_MSC_VER)
        if (cpu.supportAVX512() &&  cpu.supportFMA())
            is = AVX512FMA;
        else
#endif
        if (cpu.supportAVX())
            if (cpu.supportFMA())
                is = AVXFMA;
            else
                is = AVX;
        else
            is = SSE;
    }

    InstrSet is;

} const instrSet{};

/*
    VEC section
*/

#include "vec-types.h"
#include "vec-utils.h"
#include "api-mcvec-vec.h"

struct VecFunPtrs
{
    VecFunPtrs()
    {
        if(instrSet.is == SSE) {
#if (AVXONLY==0)
            OuterPtrSet<2, false, false>::init(p);
#else
            assert(0);
#endif
            return;
        }

        unsigned len;
        bool fma;
        switch(instrSet.is) {
            case AVX:       len = 4; fma = false; break;
            case AVXFMA:    len = 4; fma = true;  break;
            case AVX512FMA: len = 8; fma = true;  break;
            default: break;
        };

        // __m256d get instantiated only in the AVX case
        if(len == 4) {
            OuterPtrSet<4, true, false>::init(p);
            if(fma)
                OuterPtrSet<4, true, true>::init(p); // override only fma related pointers
            return;
        }

        if(len == 8) {  // len = 8. __m512d is instantiated only for AVX512. it implies FMA is available
            OuterPtrSet<8, true, true>::init(p); // init all pointers
            return;
        }
    }

    OuterPtr p;
} const vecptrs{};

const char *mcvec_instr_set()
{
    return isnames[instrSet.is];
}

template <fun_enum F>
struct Dispatcher
{
    template <typename...Args >
    FORCE_INLINE static void run(Args...args)
    {
        (*get<F>(vecptrs.p))(args...);
    }
};

#include "gen-vec-fun-dispatch.h"


/*
    mcmcpy for RHEL5
*/

#if defined(__clang__) && defined(RHLE5)

#include <cstddef>
#include <cstring>

/* some systems do not have newest memcpy@@GLIBC_2.14 - stay with old good one */
asm (".symver memcpy, memcpy@GLIBC_2.2.5");

void *__wrap_memcpy(void *dest, const void *src, std::size_t n)
{
    return std::memcpy(dest, src, n);
}

#endif // rhel5
