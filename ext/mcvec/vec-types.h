#pragma once

#include "forceinline.h"
#include "typedef.h"

typedef const double *vector_arg_t;
typedef double  scalar_arg_t;

// unart op codes
enum una_codes
    {
#include "auto/una-enum.h"
        una_last
    };

// binary op codes
enum bin_codes
    {
#include "auto/bin-enum.h"
        bin_last
    };

// ternary op codes
enum ter_codes
{
#include "auto/ter-enum.h"
    ter_last
};

enum fun_enum {
#include "gen-vec-fun-enum.h"
};

template <fun_enum> struct OuterPtrTraits;
#include "gen-vec-fun-types.h"

template <fun_enum F>
using OuterPtrType = typename OuterPtrTraits<F>::type_t;

struct OuterPtr
{
#include "gen-vec-fun-members.h"
};

template <fun_enum F>
inline OuterPtrType<F>& get(OuterPtr&);
template <fun_enum F>
inline const OuterPtrType<F>& get(const OuterPtr&);
#include "gen-vec-fun-get.h"

template <unsigned SimdLen, bool UseAVX, bool UseFMA>
struct OuterPtrSet
{
    static void init(OuterPtr&);
};

template <fun_enum> struct LoopPtrTraits;
#include "gen-vec-loop-types.h"
template <fun_enum F>
using LoopPtrType = typename LoopPtrTraits<F>::type_t;

struct LoopPtr
{
#include "gen-vec-loop-members.h"
};

template <fun_enum F>
inline LoopPtrType<F>& get(LoopPtr&);
#include "gen-vec-loop-get.h"

// return operation enum from fun code
template <fun_enum> struct fun_id;
#include "gen-vec-fun-id.h"
