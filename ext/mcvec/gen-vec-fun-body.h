#include "vec-names.h"

#define CALL(name,m,...) LoopFun<MCVEC_ENUMNAME(name,m),VecLen,UseAVX,UseFMA>::f(n, dst, __VA_ARGS__)
#define FWD(name,m,...) OuterFun<MCVEC_ENUMNAME(name,m),VecLen,UseAVX,UseFMA>::f(n, dst,__VA_ARGS__)
#define FUN_HEAD(rt, ...)  static void f(vec_size_t n, rt dst, __VA_ARGS__)

#define MCVEC_FUN(name, mode, head, ...) \
   template <unsigned VecLen, bool UseAVX, bool UseFMA> MCVEC_INDENT(0) \
   struct OuterFun<MCVEC_ENUMNAME(name,mode),VecLen,UseAVX,UseFMA> { MCVEC_INDENT(4) \
       head MCVEC_INDENT(4) \
            __VA_ARGS__ \
   MCVEC_INDENT(0)};

#define MCVEC_FUN1(n,m,body, rt, at) MCVEC_FUN(n,m, FUN_HEAD(rt, at x1), body)
#define MCVEC_FUN2(n,m,body,rt, at, bt) MCVEC_FUN(n,m, FUN_HEAD(rt, at x1, bt x2), body)
#define MCVEC_FUN3(n,m,body,rt, at, bt, ct) MCVEC_FUN(n,m, FUN_HEAD(rt, at x1, bt x2, ct x3), body)

#include "auto/all-fun.h"

#undef CALL
#undef FWD
#undef FUN_HEAD
