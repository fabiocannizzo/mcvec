#include "vec-names.h"
#define MCVEC_FUN(n,m, ty) template <> struct fun_id<MCVEC_ENUMNAME(n, m)> { static constexpr ty id = MCVEC_ID(n); };
#define MCVEC_FUN1(n,m,...) MCVEC_FUN(n,m,una_codes)
#define MCVEC_FUN2(n,m,...) MCVEC_FUN(n,m,bin_codes)
#define MCVEC_FUN3(n,m,...) MCVEC_FUN(n,m,ter_codes)

#include "auto/all-fun.h"
