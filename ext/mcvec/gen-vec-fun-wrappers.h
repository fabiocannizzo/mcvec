#include "vec-names.h"

#define MCVEC_ARG(n) arg ## n ## _t
#define MCVEC_TOSTR(n,m) #n "_" #m
#define MCVEC_ARGS(...) (n, r, __VA_ARGS__)
#define MCVEC_DISP(...) dispatch(vec_size_t n, ret_t r, __VA_ARGS__)
#define MCVEC_ARGT(at,i) typedef at MCVEC_ARG(i);MCVEC_INDENT(4)
#define MCVEC_ARG1T(at) MCVEC_ARGT(at,1)
#define MCVEC_ARG2T(at,bt) MCVEC_ARGT(at,1) MCVEC_ARGT(bt,2)
#define MCVEC_ARG3T(at,bt,ct) MCVEC_ARGT(at,1) MCVEC_ARGT(bt,2) MCVEC_ARGT(ct,3)

#define MCVEC_FUN(na, nm, md, rt, args, disp, argt) \
    template <> MCVEC_INDENT(0)\
    struct Op<MCVEC_ID(nm), md> MCVEC_INDENT(0)\
    { MCVEC_INDENT(4)\
        typedef rt ret_t;MCVEC_INDENT(4)\
        static const unsigned nargs = na;MCVEC_INDENT(4)\
        argt \
        static const char *name() { return MCVEC_TOSTR(nm,md); }MCVEC_INDENT(4)\
        FORCE_INLINE static void disp MCVEC_INDENT(4)\
        {MCVEC_INDENT(8)\
            MCVEC_FUNNAME(nm,md)args; MCVEC_INDENT(4)\
        }MCVEC_INDENT(0)\
    };

#define MCVEC_FUN1(n,m,body,rt,at) MCVEC_FUN(1,n,m,rt,MCVEC_ARGS(x1),MCVEC_DISP(arg1_t x1),MCVEC_ARG1T(at))
#define MCVEC_FUN2(n,m,body,rt,at,bt) MCVEC_FUN(2,n,m,rt,MCVEC_ARGS(x1, x2),MCVEC_DISP(arg1_t x1, arg2_t x2),MCVEC_ARG2T(at, bt))
#define MCVEC_FUN3(n,m,body,rt,at,bt,ct) MCVEC_FUN(3,n,m,rt,MCVEC_ARGS(x1, x2, x3),MCVEC_DISP(arg1_t x1, arg2_t x2, arg3_t x3),MCVEC_ARG3T(at, bt, ct))

#include "auto/all-fun.h"

#undef MCVEC_TOSTR
#undef MCVEC_ARGS
#undef MCVEC_ARG
#undef MCVEC_DISP
#undef MCVEC_ARG1T
#undef MCVEC_ARG2T
#undef MCVEC_ARG3T
#undef MCVEC_ARGT
