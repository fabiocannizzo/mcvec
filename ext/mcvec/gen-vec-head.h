#include "vec-names.h"
#define MCVEC_FUN(n,m,rt,...) MCVEC_DLL void MCVEC_FUNNAME(n,m)(vec_size_t, rt dst, __VA_ARGS__);MCVEC_INDENT(0)
#define MCVEC_FUN1(n,m,body,rt,at) MCVEC_FUN(n,m,rt,at x1)
#define MCVEC_FUN2(n,m,body,rt,at,bt) MCVEC_FUN(n,m,rt,at x1, bt x2)
#define MCVEC_FUN3(n,m,body,rt,at,bt,ct) MCVEC_FUN(n,m,rt,at x1, bt x2, ct x3)

// eliminate typedefs, this is just a C interface
#define c_num_vec_t const double *
#define c_log_vec_t const double *
#define num_vec_t double *
#define log_vec_t double *
#define vec_size_t unsigned long
#define num_cst_t double
#define log_cst_t bool

#include "auto/all-fun.h"

#undef c_num_vec_t
#undef c_log_vec_t
#undef num_vec_t
#undef log_vec_t
#undef vec_size_t
#undef num_cst_t
#undef log_cst_t
