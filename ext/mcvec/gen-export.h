#include "vec-names.h"

#define MCVEC_JOIN2(a,b) a ## b
#define MCVEC_JOIN(a,b) MCVEC_JOIN2(a,b)
#define MCVEC_FUN1(n,m,...) MCVEC_FUNNAME(n,m);
#define MCVEC_FUN2(n,m,...) MCVEC_FUNNAME(n,m);
#define MCVEC_FUN3(n,m,...) MCVEC_FUNNAME(n,m);

MCVEC_JOIN(MCVEC_,MCVEC_VERSION) {
 global:

 mcvec_instr_set;
#include "auto/all-fun.h"

#define MCVEC_POOL_GEN(r,c) MCVEC_POOL_FUN(max, r, c, fwd); MCVEC_INDENT(0) \
    MCVEC_POOL_FUN(max, r, c, bwd);

 local: *;
};
