//
// code adapted from:
// https://github.com/Mysticial/FeatureDetector
// Author:  Alexander J. Yee
//

#include "cpu.h"

#if defined(_MSC_VER) || defined (__INTEL_COMPILER)
#include <intrin.h>
#elif defined(__GNUC__)
#include <cpuid.h>
#endif

#define _XCR_XFEATURE_ENABLED_MASK 0

void CPUFeatures::cpuid(int output[4], int funid)
{
#   if defined(__GNUC__)
        __cpuid_count(funid, 0, output[0], output[1], output[2], output[3]);
#   elif defined (_MSC_VER) || defined (__INTEL_COMPILER)     // Microsoft or Intel compiler, intrin.h included
        __cpuidex(output, funid, 0);                  // intrinsic function for CPUID
#   else
#      error What compiler is this?
#   endif
}

uint64_t CPUFeatures::xgetbv(int ctr)
{
#   if defined (_MSC_VER) || defined (__INTEL_COMPILER)
        return _xgetbv(ctr);
#   elif defined(__GNUC__)
        uint32_t a, d;
        __asm("xgetbv" : "=a"(a), "=d"(d) : "c"(ctr) : );
        return a | (uint64_t(d) << 32);
#   else
#       error What compiler is this?
#   endif
}

CPUFeatures::CPUFeatures()
    : m_SSE41(false), m_SSE42(false), m_AVX(false), m_FMA3(false), m_AVX2(false)
    , m_AVX512_F(false), m_AVX512_DQ(false), m_FMA4(false)
    , m_osUsesXSAVE_XRSTORE(false), m_ymm_save(false), m_zmm_save(false)
{
    int info[4];
    cpuid(info, 0);
    int nIds = info[0];

    cpuid(info, 0x80000000);
    uint32_t nExIds = info[0];

    //  Detect Features
    if(nIds >= 0x00000001) {
        cpuid(info, 0x00000001);
        //m_MMX = (info[3] & ((int)1 << 23)) != 0;
        //m_SSE = (info[3] & ((int)1 << 25)) != 0;
        //m_SSE2 = (info[3] & ((int)1 << 26)) != 0;
        //m_SSE3 = (info[2] & ((int)1 << 0)) != 0;

        //m_SSSE3 = (info[2] & ((int)1 << 9)) != 0;
        m_SSE41 = (info[2] & ((int)1 << 19)) != 0;
        m_SSE42 = (info[2] & ((int)1 << 20)) != 0;
        //m_AES = (info[2] & ((int)1 << 25)) != 0;

        m_osUsesXSAVE_XRSTORE = (info[2] & ((int)1 << 27)) != 0;
        m_AVX = (info[2] & ((int)1 << 28)) != 0;
        m_FMA3 = (info[2] & ((int)1 << 12)) != 0;

        if(m_osUsesXSAVE_XRSTORE && m_AVX) {
            uint64_t xcrFeatureMask = xgetbv(_XCR_XFEATURE_ENABLED_MASK);
            m_ymm_save = (xcrFeatureMask & 0x6) == 0x6;
        }

        //m_RDRAND = (info[2] & ((int)1 << 30)) != 0;
    }
    if(nIds >= 0x00000007) {
        cpuid(info, 0x00000007);
        m_AVX2 = (info[1] & ((int)1 << 5)) != 0;

        //m_BMI1 = (info[1] & ((int)1 << 3)) != 0;
        //m_BMI2 = (info[1] & ((int)1 << 8)) != 0;
        //m_ADX = (info[1] & ((int)1 << 19)) != 0;
        //m_MPX = (info[1] & ((int)1 << 14)) != 0;
        //m_SHA = (info[1] & ((int)1 << 29)) != 0;
        //m_PREFETCHWT1 = (info[2] & ((int)1 << 0)) != 0;

        // avx512 detection

        m_AVX512_F = (info[1] & ((int)1 << 16)) != 0;

        if(m_ymm_save && m_AVX512_F) {
            uint64_t xcrFeatureMask = xgetbv(_XCR_XFEATURE_ENABLED_MASK);
            m_zmm_save = (xcrFeatureMask & 0xe6) == 0xe6;
        }

        //m_AVX512_CD = (info[1] & ((int)1 << 28)) != 0;
        //m_AVX512_PF = (info[1] & ((int)1 << 26)) != 0;
        //m_AVX512_ER = (info[1] & ((int)1 << 27)) != 0;
        //m_AVX512_VL = (info[1] & ((int)1 << 31)) != 0;
        //m_AVX512_BW = (info[1] & ((int)1 << 30)) != 0;
        m_AVX512_DQ = (info[1] & ((int)1 << 17)) != 0;
        //m_AVX512_IFMA = (info[1] & ((int)1 << 21)) != 0;
        //m_AVX512_VBMI = (info[2] & ((int)1 << 1)) != 0;
    }
    if(nExIds >= 0x80000001) {
        cpuid(info, 0x80000001);
        //m_x64 = (info[3] & ((int)1 << 29)) != 0;
        //m_ABM = (info[2] & ((int)1 << 5)) != 0;
        //m_SSE4a = (info[2] & ((int)1 << 6)) != 0;
        m_FMA4 = (info[2] & ((int)1 << 16)) != 0;
        //m_XOP = (info[2] & ((int)1 << 11)) != 0;
    }
}
