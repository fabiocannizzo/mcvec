# Possible options:
# DEBUG                        # [ 0 | 1 ],         default 0
# COMPILER                     # [ g++ | clang++ ], default: clang (C++17 fully compliant compiler required, which also allow to control loop unrolling)
# CROSS                        # [ i686-w64-mingw32- | x86_64-w64-mingw32- ], default: i686-w64-mingw32- (used in cygwin to compile for Windows)
# RHLE5                        # [ 0 | 1 ], default 1 on Linux, 0 otherwise
# AVXONLY                      # [ 0 | 1 ], default 0: use AVX instead of SSE as lowest possible instruction set
#

# Command line examples:
# 1) target win64, without sse support (allow avx only), on cygwin
#    make AVXONLY=1 CROSS=x86_64-w64-mingw32-
# 2) target win32, with sse support, on cygwin
#    make AVXONLY=0 CROSS=i686-w64-mingw32-


SKIP_AVX512 ?= 0
AVXONLY ?= 1

VERSION = 2_4

# platform
PLATFORM := $(shell uname -s)

ifneq (Linux,$(PLATFORM))
   CROSS ?= i686-w64-mingw32-
endif

ifeq (mingw,$(findstring mingw,$(CROSS)))
   ifeq (,$(shell which lib 2>/dev/null))
       $(error 'lib', the MSVC tool to create import libraries, must be available via the PATH when using mingw)
   endif
   ifeq ($(CROSS), i686-w64-mingw32-)
       IS_MINGW := 1
       MINGW := 32
       WINLIB := X86
   else ifeq ($(CROSS), x86_64-w64-mingw32-)
       IS_MINGW := 1
       MINGW := 64
       WINLIB := X64
   else
        $(error weird mingw string defined)
   endif
else
   IS_MINGW := 0
endif


# compiler
COMPILER ?= g++
OBJCC = $(CROSS)$(COMPILER)
CC = $(CROSS)g++

ifeq (Linux,$(PLATFORM))
   RHLE5=1
endif


# file extensions
ifeq ($(PLATFORM), Linux)
  OBJ=.o
  EXE=
  DLL=.so
  LIB=.a
else
# cygwin or mingw
  OBJ=.obj
  EXE=.exe
  DLL=.dll
  LIB=.lib
endif

#
# FILE LISTS
#

DLLDIRNAME := mcvec_$(VERSION)
ifeq ($(IS_MINGW),1)
    ifeq ($(MINGW),64)
        ARCH := w64
    else
        ARCH := w32
    endif
else
    ARCH := l64
endif

ifeq ($(AVXONLY),1)
    SIMD := avx
else
    SIMD := sse
endif
DLLNAME := $(DLLDIRNAME)-$(ARCH)-$(SIMD)


# directories
AUTODIR=auto
GENDIR=gen
HDIR=include
BINDIRPOSTFIX=$(OBJCC)-$(ARCH)-$(SIMD)
ifdef DEBUG
   BINDIR:=bin-debug-$(BINDIRPOSTFIX)
else
   BINDIR:=bin-$(BINDIRPOSTFIX)
endif


# print infos
$(info platform = $(PLATFORM))
$(info compiler = $(COMPILER))
$(info is mingw = $(IS_MINGW))
$(info ARCH = $(ARCH))
$(info libmode = $(WINLIB))
$(info rhle5 = $(RHLE5))
$(info avx only = $(AVXONLY))
$(info bindir = $(BINDIR))
$(info dllname = $(DLLNAME))


# Do not need to list all auto files. One is enough to trigger all recalculation.
LIBHDR:=$(wildcard *.h)
SHSRC:=$(wildcard *.sh)
CSVSRC:=$(wildcard *.csv)
ENUMS:=$(patsubst x-%-codes.csv, %, $(CSVSRC))
EXESRC:=$(wildcard test*.cpp)
GENHDR:=$(patsubst gen-%.h, $(GENDIR)/%.h, $(wildcard gen-*.h))
NAMEHDR:=$(wildcard *-names.h)
SEEDHDR:=$(AUTODIR)/all-fun.h $(wildcard ml-*-all.h)
#DELEMPTY:=export fun-types fun-enum fun-members fun-get fun-head fun-boqdy
ifeq ($(AVXONLY),0)
    VECSRC := $(wildcard vec-*.cpp)
    MLSRC  := $(wildcard ml-*.cpp)
else
    VECSRC := $(filter-out vec-sse.cpp,$(wildcard vec-*.cpp))
    MLSRC  := $(filter-out ml-see.cpp,$(wildcard ml-*.cpp))
    $(info $(VECSRC))
endif
VECOBJ:=$(patsubst %.cpp, $(BINDIR)/%$(OBJ), $(VECSRC))
MLOBJ:=$(patsubst %.cpp, $(BINDIR)/%$(OBJ), $(MLSRC))
DLLOBJ:=$(patsubst %, $(BINDIR)/%$(OBJ), main cpu) $(VECOBJ) $(MLOBJ)
EXEOBJ:=$(patsubst %.cpp, $(BINDIR)/%$(OBJ), $(EXESRC))
EXETARGET:=$(patsubst %$(OBJ), %$(EXE), $(EXEOBJ))
DLLTARGET:=$(BINDIR)/$(DLLNAME)$(DLL)
DLLLIB:=$(patsubst %$(DLL),%$(LIB), $(DLLTARGET))

#
# FLAGS
#

ifeq ($(AVXONLY),1)
    MINSET = -mavx
else
    MINSET = -msse4.2
endif

CFLAGS :=-c -std=c++17 -DSKIP_AVX512=$(SKIP_AVX512) -DAVXONLY=$(AVXONLY) $(MINSET)
LFLAGS :=

ifeq ($(IS_MINGW),0)
    $(DLLOBJ) : CFLAGS += -fPIC
endif
$(VECOBJ) $(MLOBJ): CFLAGS += $(DLLCFLAGS) -save-temps=obj # -masm=intel
$(DLLTARGET) : LFLAGS +=-Wl,--version-script=$(BINDIR)/export.version
ifeq ($(IS_MINGW),1)
   ifeq ($(ARCH),w32)
       CFLAGS += -march=i686
       ifeq ($(SIMD),sse)
           # do not use 80x87 coprocessor
           CFLAGS += -mfpmath=sse
       endif
   endif
   LFLAGS += -static
   $(DLLTARGET): LFLAGS +=-Wl,--output-def,$(BINDIR)/$(DLLNAME).def
else
   ifeq ($(RHLE5),1)
       $(DLLTARGET) : LFLAGS +=-Wl,--wrap=memcpy
       $(BINDIR)/main$(OBJ) : CFLAGS+=-DRHLE5=1
   endif
endif

ifdef DEBUG
     CFLAGS += -g -DDEBUG -O0
else
     CFLAGS += -O3
     LFLAGS += -O3
     $(DLLTARGET) : LFLAGS +=-Wl,--strip-all
endif

$(BINDIR)/%-avx$(OBJ) : CFLAGS:=$(subst $(MINSET),-mavx,$(CFLAGS))
$(BINDIR)/%-avxfma$(OBJ) : CFLAGS:=$(subst $(MINSET),-mavx -mfma,$(CFLAGS))
$(BINDIR)/%-avx512fma$(OBJ) : CFLAGS:=$(subst $(MINSET),-mavx512dq -mfma,$(CFLAGS))

# used in auto generation
POST = | sed '/^[ ]*$$/d'
FORMAT = | perl -ne 's/\s*\$$\((\d+)\)\h*(\R)?/"\n"." "x$$1.$$2/eg;print'
STRIPBEGIN = | sed -ne '/generated/,$$ p'
AUTOGEN=This is an auto generated file
BEGINAPI=BEGIN-API

#
# RULES
#

# disable all default rules
.SUFFIXES:
# do not delete intermediate files
.SECONDARY:

all : $(DLLTARGET) $(EXETARGET)
gen : $(GENHDR)
api : $(patsubst %, $(HDIR)/%.h, mcvec-vec mcvec-ml)

# header files
$(HDIR)/%.h : api-%.cpp $(LIBHDR) $(AUTODIR)/all-fun.h Makefile | dirs
	cpp -C -P -DUSE_INDENT $< $(FORMAT) | sed -e '/$(BEGINAPI)/,$$!d' -e '/$(BEGINAPI)/d' -e '/$(AUTOGEN)/d' | sed -e '1i#pragma once\n\/\/ $(AUTOGEN)\n' >  $(HDIR)/$*.h

# auxiliary files
$(AUTODIR)/%.h : $(CSVSRC) $(SHSRC) $(BINDIR)/parser$(EXE) Makefile | dirs
	$(BINDIR)/parser$(EXE) $(ENUMS)

# files auto-generated via the preprocessor
# for ml files we do not run STRIPBEGIN
$(GENDIR)/ml-%.h : STRIPBEGIN =
$(GENDIR)/%.h : gen-%.h $(SEEDHDR) $(NAMEHDR) Makefile | dirs
	cpp -C -P -DUSE_INDENT $< $(STRIPBEGIN) $(POST) $(FORMAT) > $@
$(BINDIR)/export.version : gen-export.h $(SEEDHDR) $(NAMEHDR) Makefile | dirs
	cpp -P -DMCVEC_VERSION=$(VERSION) $< $(POST) > $@

# object files
$(BINDIR)/%$(OBJ) : %.cpp $(LIBHDR) $(AUTODIR)/all-fun.h Makefile | dirs
	$(OBJCC) $(CFLAGS) $< -o $@

# test executables
LINKLIB=$(DLLTARGET)

$(BINDIR)/%$(EXE) : $(BINDIR)/%$(OBJ) $(DLLTARGET)
	$(CC) $(LFLAGS) $< $(LINKLIB) -o $@

# parser
$(BINDIR)/parser$(EXE) : parser.cpp Makefile | dirs
	g++ $< -o $@

# dll and import library
$(DLLTARGET) : $(DLLOBJ) $(BINDIR)/export.version | dirs
	$(CC) -shared $(LFLAGS) $(DLLOBJ) -o $@
ifeq ($(IS_MINGW),1)
	lib /machine:$(WINLIB) /def:$(BINDIR)/$(DLLNAME).def /name:$(DLLNAME) /out:$(DLLLIB)
endif


ifeq ($(IS_MINGW),1)
TARGETDIR=/workspace/ext/$(DLLDIRNAME)
cortex: $(DLLTARGET) api
	mkdir -p $(TARGETDIR)
	cp $(DLLTARGET) $(BINDIR)/$(DLLNAME)$(LIB) $(TARGETDIR)
	cp include/* $(TARGETDIR)
	[ "$(DIST)" != "" ] && cp $(DLLTARGET) $(DIST)
endif

LINUXTARGETDIR=/scratch/cortexdev/ext/x86_64/$(DLLNAME)
PKGDIR=$(BINDIR)/$(DLLNAME)
linux: $(DLLTARGET) api
	mkdir -p $(PKGDIR)
	cp include/* $(PKGDIR)
	cp $(DLLTARGET) $(PKGDIR)/lib$(DLLNAME).so
#	ssh -t cortexdev@dev1 mkdir -p $(LINUXTARGETDIR)
#	scp $(TARGETDLL) cortexdev@dev1:$(LINUXTARGETDIR)/lib$(DLLNAME).so
#	scp include/* $(LINUXTARGETDIR)

# clean
.PHONY: clean
clean:
	rm -rf $(AUTODIR) $(GENDIR) $(HDIR) bin-*


# use -p for multithreading
dirs:
	mkdir -p $(BINDIR) $(AUTODIR) $(GENDIR) $(HDIR)
