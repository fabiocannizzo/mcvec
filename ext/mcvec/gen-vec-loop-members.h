#include "vec-names.h"
#define MCVEC_FUN(n,m) MCVEC_LOOPTYPENAME(n, m) MCVEC_MEMBERNAME(n,m) = nullptr;
#define MCVEC_FUN1(n,m,...) MCVEC_FUN(n,m)
#define MCVEC_FUN2(n,m,...) MCVEC_FUN(n,m)
#define MCVEC_FUN3(n,m,...) MCVEC_FUN(n,m)

#include "auto/all-loop.h"
