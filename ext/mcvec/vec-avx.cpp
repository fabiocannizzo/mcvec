#if !defined(__AVX__) || defined(__AVX2__) || defined(__FMA__)
#error This file must be compiled with avx instructions
#endif

#include "vec-xxx.h"

template struct OuterPtrSet<4, true, false>; // force instantiation
