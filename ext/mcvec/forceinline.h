#pragma once

// define FORCE_INLINE
#ifndef FORCE_INLINE
#if defined(DEBUG) || defined(_DEBUG)
#   if defined(__GNUC__)
#       // define FORCE_INLINE __attribute__((never_inline))
#       define FORCE_INLINE
#   elif defined(_MSC_VER)
#       define FORCE_INLINE __declspec(noinline)
#   endif
#else
#   if defined(__GNUC__)
#define FORCE_INLINE __attribute__((always_inline))
#   elif defined(_MSC_VER) || defined(__INTEL_COMPILER)
#	    define FORCE_INLINE __forceinline
#   else
#	    error What compiler is this?
#   endif
#endif
#endif

