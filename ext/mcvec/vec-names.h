#pragma once
#define MCVEC_ENUMNAME(name,mode) id_ ## name ## _ ## mode
#define MCVEC_OUTERTYPENAME(name,mode) OuterPtrType<MCVEC_ENUMNAME(name,mode)>
#define MCVEC_LOOPTYPENAME(name,mode) LoopPtrType<MCVEC_ENUMNAME(name,mode)>
#define MCVEC_FUNNAME(name,mode) mcvec_ ## name ## _ ## mode
#define MCVEC_OUTERNAME(name,mode) outer_ ## name ## _ ## mode
#define MCVEC_MEMBERNAME(name,mode) m_ ## name ## _ ## mode
#define MCVEC_ID(name) id_ ## name
#define MCVEC_CODE(name) decltype(mkcode<MCVEC_ID(name)>())

#ifdef USE_INDENT
#   define MCVEC_INDENT(n) $(n)
#endif

