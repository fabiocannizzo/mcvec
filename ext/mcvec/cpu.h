//
// code adapted from:
// https://github.com/Mysticial/FeatureDetector
// Author:  Alexander J. Yee
//

#include <cstdint>

class CPUFeatures
{
    // data members
    bool m_SSE41, m_SSE42, m_AVX, m_FMA3, m_AVX2, m_AVX512_F, m_AVX512_DQ, m_FMA4, m_osUsesXSAVE_XRSTORE, m_ymm_save, m_zmm_save;

    // auxilary functions
    static void cpuid(int output[4], int funid);
    static uint64_t xgetbv(int ctr);

public:
    CPUFeatures();

    bool supportAVX() const
    {
        return m_AVX && m_ymm_save && m_osUsesXSAVE_XRSTORE;
    }

    bool supportFMA() const
    {
        return m_FMA3;
    }

    bool supportAVX512() const
    {
        return m_AVX512_DQ && m_zmm_save && m_osUsesXSAVE_XRSTORE;
    }
};
