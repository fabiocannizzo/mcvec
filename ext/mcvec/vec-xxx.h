#include "vec-types.h"
#include "vec-utils.h"

#include <cstdint>
#include <cmath>
#include <algorithm>
#include <type_traits>
#include <limits>

#include <immintrin.h>

using namespace std;

template <unsigned N>
struct SimdOpsBase;

// scalar ops
template <>
struct SimdOpsBase<1>
{
    typedef double arg_t;

    FORCE_INLINE static arg_t zero() { return 0.0; }
    FORCE_INLINE static arg_t lift(arg_t x) { return x; }

    template <bool Aligned>
    FORCE_INLINE static arg_t load(vector_arg_t src) { return *src; }
    template <bool Aligned>
    FORCE_INLINE static void store(double *dest, arg_t value) { *dest = value; }

protected:

    template <una_codes C>
    FORCE_INLINE static arg_t fun(arg_t x)
    {
        switch(C) {
            case id_sqrt: { __m128d vx = _mm_set_sd(x); return _mm_cvtsd_f64(_mm_sqrt_sd(vx,vx)); }
            default: return lift(numeric_limits<double>::quiet_NaN());
        }
    }

    template <bin_codes C>
    FORCE_INLINE static arg_t fun(arg_t x, arg_t y)
    {
        switch(C) {
            case id_add: return x + y;
            case id_sub: return x - y;
            case id_mul: return x * y;
            case id_div: return x / y;
            case id_min: return x < y ? x : y;
            case id_max: return y < x ? x : y;

            case id_and: return i2d(d2i(x) & d2i(y));
            case id_andnot1: return i2d(~d2i(x) & d2i(y));
            case id_or: return i2d(d2i(x) | d2i(y));
            case id_xor: return i2d(d2i(x) ^ d2i(y));

#define scalar_bin_op(f, x, y) _mm_cvtsd_f64(f(_mm_set_sd(x), _mm_set_sd(y)))
            case id_ge: return scalar_bin_op(_mm_cmpge_sd, x, y);
            case id_gt: return scalar_bin_op(_mm_cmpgt_sd, x, y);
            case id_le: return scalar_bin_op(_mm_cmple_sd, x, y);
            case id_lt: return scalar_bin_op(_mm_cmplt_sd, x, y);
            case id_eq: return scalar_bin_op(_mm_cmpeq_sd, x, y);
            case id_ne: return scalar_bin_op(_mm_cmpneq_sd, x, y);

            default: return lift(numeric_limits<double>::quiet_NaN());
        }
    }

    template <ter_codes C>
    FORCE_INLINE static arg_t fun(arg_t x, arg_t y, arg_t z)
    {
        switch(C) {
            case id_ite: return x ? y : z;
            case id_muladd: return x * y + z;
            case id_mulsub: return x * y - z;

            default: return lift(numeric_limits<double>::quiet_NaN());
        }
    }
};

// scalar SSE ops
template <>
struct SimdOpsBase<2>
{
    typedef __m128d arg_t;

    FORCE_INLINE static arg_t zero() { return _mm_setzero_pd(); }
    FORCE_INLINE static arg_t lift(double x) { return _mm_set1_pd( x ); }

    template <bool Aligned>
    FORCE_INLINE static arg_t load(const double *src) { return Aligned ? _mm_load_pd( src ) : _mm_loadu_pd( src ); }
    template <bool Aligned>
    FORCE_INLINE static void store(double *dest, arg_t value)
    {
        if constexpr (Aligned)
            _mm_store_pd(dest, value);
        else
            _mm_storeu_pd(dest, value);
    }

protected:

    template <una_codes C>
    FORCE_INLINE static arg_t fun(arg_t x)
    {
        switch(C) {
            case id_sqrt: return _mm_sqrt_pd(x);

            default: return lift(numeric_limits<double>::quiet_NaN());
        }
   }

    template <bin_codes C>
    FORCE_INLINE static arg_t fun(arg_t x, arg_t y)
    {
        switch(C) {
            case id_add: return _mm_add_pd(x, y);
            case id_sub: return _mm_sub_pd(x, y);
            case id_mul: return _mm_mul_pd(x, y);
            case id_div: return _mm_div_pd(x, y);
            case id_min: return _mm_min_pd(x, y);
            case id_max: return _mm_max_pd(x, y);

            case id_and: return _mm_and_pd(x, y);
            case id_andnot1: return _mm_andnot_pd(x, y);
            case id_or: return _mm_or_pd(x, y);
            case id_xor: return _mm_xor_pd(x, y);

            case id_ge: return _mm_cmpge_pd(x, y);
            case id_gt: return _mm_cmpgt_pd(x, y);
            case id_le: return _mm_cmple_pd(x, y);
            case id_lt: return _mm_cmplt_pd(x, y);
            case id_eq: return _mm_cmpeq_pd(x, y);
            case id_ne: return _mm_cmpneq_pd(x, y);

            default: return lift(numeric_limits<double>::quiet_NaN());
        }
    }

    template <ter_codes C>
    FORCE_INLINE static arg_t fun(arg_t x, arg_t y, arg_t z)
    {
        switch(C) {
            case id_ite: return _mm_blendv_pd(z, y, x);
#ifdef __FMA__
            case id_muladd: return _mm_fmadd_pd(x, y, z);
            case id_mulsub: return _mm_fmsub_pd(x, y, z);
#else
            case id_muladd: return fun<id_add>(fun<id_mul>(x, y), z);
            case id_mulsub: return fun<id_sub>(fun<id_mul>(x, y), z);
#endif
            default: return lift(numeric_limits<double>::quiet_NaN());
        }
    }
};

#ifdef __AVX__
// scalar AVX ops
template <>
struct SimdOpsBase<4>
{
    typedef __m256d arg_t;

    FORCE_INLINE static arg_t zero() { return _mm256_setzero_pd(); }
    FORCE_INLINE static arg_t lift(double x) { return _mm256_set1_pd( x ); }

    template <bool Aligned>
    FORCE_INLINE static arg_t load(const double *src) { return Aligned ? _mm256_load_pd( src ) : _mm256_loadu_pd( src ); }
    template <bool Aligned>
    FORCE_INLINE static void store(double *dest, arg_t value)
    {
        if constexpr (Aligned)
            _mm256_store_pd( dest, value);
        else
            _mm256_storeu_pd( dest, value);
    }

protected:

    template <una_codes C>
    FORCE_INLINE static arg_t fun(arg_t x)
    {
        switch(C) {
            case id_sqrt: return _mm256_sqrt_pd(x);
            default: return lift(numeric_limits<double>::quiet_NaN());
        }
    }

    template <bin_codes C>
    FORCE_INLINE static arg_t fun(arg_t x, arg_t y)
    {
        switch(C) {
            case id_add: return _mm256_add_pd(x, y);
            case id_sub: return _mm256_sub_pd(x, y);
            case id_mul: return _mm256_mul_pd(x, y);
            case id_div: return _mm256_div_pd(x, y);
            case id_min: return _mm256_min_pd(x, y);
            case id_max: return _mm256_max_pd(x, y);

            case id_and: return _mm256_and_pd(x, y);
            case id_andnot1: return _mm256_andnot_pd(x, y);
            case id_or: return _mm256_or_pd(x, y);
            case id_xor: return _mm256_xor_pd(x, y);

            case id_ge: return _mm256_cmp_pd(x, y, _CMP_GE_OQ);
            case id_gt: return _mm256_cmp_pd(x, y, _CMP_GT_OQ);
            case id_le: return _mm256_cmp_pd(x, y, _CMP_LE_OQ);
            case id_lt: return _mm256_cmp_pd(x, y, _CMP_LT_OQ);
            case id_eq: return _mm256_cmp_pd(x, y, _CMP_EQ_OQ);
            case id_ne: return _mm256_cmp_pd(x, y, _CMP_NEQ_OQ);
            default: return lift(numeric_limits<double>::quiet_NaN());
        }
    }

    template <ter_codes C>
    FORCE_INLINE static arg_t fun(arg_t x, arg_t y, arg_t z)
    {
        switch(C) {
            case id_ite: return _mm256_blendv_pd(z, y, x);
#ifdef __FMA__
            case id_muladd: return _mm256_fmadd_pd(x, y, z);
            case id_mulsub: return _mm256_fmsub_pd(x, y, z);
#else
            case id_muladd: return fun<id_add>(fun<id_mul>(x, y), z);
            case id_mulsub: return fun<id_sub>(fun<id_mul>(x, y), z);
#endif
            default: return lift(numeric_limits<double>::quiet_NaN());
        }
    }
};
#endif

#ifdef __AVX512DQ__
// scalar AVX ops
template <>
struct SimdOpsBase<8>
{
    typedef __m512d arg_t;

    FORCE_INLINE static arg_t zero() { return _mm512_setzero_pd(); }
    FORCE_INLINE static arg_t lift(double x) { return _mm512_set1_pd( x ); }

    template <bool Aligned>
    FORCE_INLINE static arg_t load(const double *src) { return Aligned ? _mm512_load_pd( src ) : _mm512_loadu_pd( src ); }
    template <bool Aligned>
    FORCE_INLINE static void store(double *dest, arg_t value)
    {
        if constexpr (Aligned)
            _mm512_store_pd( dest, value);
        else
            _mm512_storeu_pd( dest, value);
    }

protected:

    template <una_codes C>
    FORCE_INLINE static arg_t fun(arg_t x)
    {
        switch(C) {
            case id_sqrt: return _mm512_sqrt_pd(x);
            default: return lift(numeric_limits<double>::quiet_NaN());
        }
    }

    template <bin_codes C>
    FORCE_INLINE static arg_t fun(arg_t x, arg_t y)
    {
        switch(C) {
            case id_add: return _mm512_add_pd(x, y);
            case id_sub: return _mm512_sub_pd(x, y);
            case id_mul: return _mm512_mul_pd(x, y);
            case id_div: return _mm512_div_pd(x, y);
            case id_min: return _mm512_min_pd(x, y);
            case id_max: return _mm512_max_pd(x, y);

            case id_and: return _mm512_and_pd(x, y);
            case id_andnot1: return _mm512_andnot_pd(x, y);
            case id_or: return _mm512_or_pd(x, y);
            case id_xor: return _mm512_xor_pd(x, y);

            case id_ge: return _mm512_castsi512_pd(_mm512_movm_epi64(_mm512_cmp_pd_mask(x, y, _CMP_GE_OQ)));
            case id_gt: return _mm512_castsi512_pd(_mm512_movm_epi64(_mm512_cmp_pd_mask(x, y, _CMP_GT_OQ)));
            case id_le: return _mm512_castsi512_pd(_mm512_movm_epi64(_mm512_cmp_pd_mask(x, y, _CMP_LE_OQ)));
            case id_lt: return _mm512_castsi512_pd(_mm512_movm_epi64(_mm512_cmp_pd_mask(x, y, _CMP_LT_OQ)));
            case id_eq: return _mm512_castsi512_pd(_mm512_movm_epi64(_mm512_cmp_pd_mask(x, y, _CMP_EQ_OQ)));
            case id_ne: return _mm512_castsi512_pd(_mm512_movm_epi64(_mm512_cmp_pd_mask(x, y, _CMP_NEQ_OQ)));

            default: return lift(numeric_limits<double>::quiet_NaN());
        }
    }

    template <ter_codes C>
    FORCE_INLINE static arg_t fun(arg_t x, arg_t y, arg_t z)
    {
        switch(C) {
            case id_ite: return _mm512_mask_blend_pd(_mm512_movepi64_mask(_mm512_castpd_si512(x)), z, y);
#ifdef __FMA__
            case id_muladd: return _mm512_fmadd_pd(x, y, z);
            case id_mulsub: return _mm512_fmsub_pd(x, y, z);
#else
            case id_muladd: return fun<id_add>(fun<id_mul>(x, y), z);
            case id_mulsub: return fun<id_sub>(fun<id_mul>(x, y), z);
#endif
            default: return lift(numeric_limits<double>::quiet_NaN());
        }
    }
};
#endif

template <unsigned N>
struct SimdOps : SimdOpsBase<N>
{
private:
    typedef SimdOpsBase<N> B;
public:
    typedef typename B::arg_t arg_t;
private:
    template <bin_codes C>
    FORCE_INLINE static arg_t f(arg_t x, arg_t y) { return B::template fun<C>(x, y); }
    template <ter_codes C>
    FORCE_INLINE static arg_t f(arg_t x, arg_t y, arg_t z) { return B::template fun<C>(x, y, z); }

public:

    template <una_codes C>
    FORCE_INLINE static arg_t fun(arg_t x)
    {
        switch(C) {
            case id_set: return x;
            case id_copy: return x;
            default: return B::template fun<C>(x);
        }
    }

    template <bin_codes C>
    FORCE_INLINE static arg_t fun(arg_t x, arg_t y)
    {
        switch(C) {
            case id_nand: return f<id_andnot1>(f<id_and>(x,y), B::lift(b2d(true)));
            case id_nor: return f<id_andnot1>(f<id_or>(x,y), B::lift(b2d(true)));
            case id_nxor: return f<id_andnot1>(f<id_xor>(x,y), B::lift(b2d(true)));
            case id_ornot1: return f<id_or>(f<id_andnot1>(x, B::lift(b2d(true))), y);

            case id_mulnz: return f<id_and>(f<id_mul>(x, y), f<id_ne>(x, B::zero()));
            case id_condincr: return f<id_add>(y, f<id_and>(x, B::lift(1.0)));

            default: return f<C>(x, y);
        }
    }

    template <ter_codes C>
    FORCE_INLINE static arg_t fun(arg_t x, arg_t y, arg_t z)
    {
        switch(C) {
            case id_add3: return f<id_add>(f<id_add>(x,y), z);
            case id_mul3: return f<id_mul>(f<id_mul>(x,y), z);
            case id_min3: return f<id_min>(f<id_min>(x, y), z);
            case id_max3: return f<id_max>(f<id_max>(x, y), z);

            case id_imult0: return f<id_and>(x, f<id_mul>(y, z));

            case id_gele: return f<id_and>(f<id_ge>(x,y), f<id_le>(x,z));
            case id_gelt: return f<id_and>(f<id_ge>(x,y), f<id_lt>(x,z));
            case id_gtle: return f<id_and>(f<id_gt>(x,y), f<id_le>(x,z));
            case id_gtlt: return f<id_and>(f<id_gt>(x,y), f<id_lt>(x,z));
            case id_ngele: return f<id_or>(f<id_lt>(x,y), f<id_gt>(x,z));
            case id_ngelt: return f<id_or>(f<id_lt>(x,y), f<id_ge>(x,z));
            case id_ngtle: return f<id_or>(f<id_le>(x,y), f<id_gt>(x,z));
            case id_ngtlt: return f<id_or>(f<id_le>(x,y), f<id_ge>(x,z));

            default: return f<C>(x, y, z);
        }
    }

};

template <typename U> constexpr bool is_fma(U C) { return false; }
template <> constexpr bool is_fma<ter_codes>(ter_codes C) { return (C == id_muladd )|| (C == id_mulsub); }

template <unsigned SimdMaxLen, bool Aligned, typename T>
struct ArgWrap;

template <unsigned SimdMaxLen, bool Aligned>
struct ArgWrap<SimdMaxLen, Aligned, vector_arg_t>
{
    ArgWrap(vector_arg_t p) : p(p) {}
    template <unsigned N>
    FORCE_INLINE typename SimdOps<N>::arg_t get(vec_size_t i) const
    {
        return SimdOps<N>::template load<Aligned>(p + i);
    }
private:
    vector_arg_t p;
};


template <unsigned SimdMaxLen, bool Aligned>
struct ArgWrap<SimdMaxLen, Aligned, double>
{
    ArgWrap(scalar_arg_t v) : v(SimdOps<SimdMaxLen>::lift(v)) {}
    template <unsigned N>
    FORCE_INLINE typename SimdOps<N>::arg_t get(vec_size_t i) const
    {
        return reinterpret_cast<const typename SimdOps<N>::arg_t&>(v);
    }
private:
    typename SimdOps<SimdMaxLen>::arg_t v;
};


// Execute the loop
template <fun_enum F, unsigned SimdMaxLen, bool Aligned>
struct LoopImpl
{
    // recursive loop unroller
    template<unsigned nItems, unsigned simdVecLen>
    struct Unroller
    {
        template<typename...WArgs>
        FORCE_INLINE static void loopblock(vec_size_t begin, double *dst, const WArgs&...wsrcs)
        {
            // process simdVecLen elements
            auto res = SimdOps<simdVecLen>::template fun<fun_id<F>::id>(wsrcs.template get<simdVecLen>(begin)...);
            SimdOps<simdVecLen>::template store<Aligned>(dst + begin, res);

            // recursive unrolling
            if constexpr (nItems > simdVecLen)
                Unroller<nItems - simdVecLen, simdVecLen>::loopblock(begin + simdVecLen, dst, wsrcs...);
        };
    };

    // Recursive loop splitter
    // Partitions the loop in sections. The first and main section is the largest multiple of block_size
    // and is processed with loop unrolling using a simd vector length of SimdMaxLen
    // The remainder is processed without loop unrolling progressively reducing the length of the simd vector
    template<unsigned nItems, unsigned simdVecLen, bool OnlyOneIter>
    struct Splitter
    {
        template<typename...Args>
        FORCE_INLINE static void loopblock(vec_size_t& begin, vec_size_t end, const Args&...args)
        {
            // iterate
            size_t nIter = (end - begin) / nItems;
            while(nIter--) {
                Unroller<nItems, simdVecLen>::loopblock(begin, args...);
                begin += nItems;
                if constexpr (OnlyOneIter) // resolved at compile time: we can only get here when peeling the last vector element, and we know that at most one iteration is required
                    break;
            }
            // Skip peeling section if the vector size is a multiple of the block_size and the
            // vector is aligned. This introduces an extra check but will improve performance for
            // perfect vectors, which should be the most common case.
            if constexpr (Aligned && (nItems > simdVecLen))
                if (end == begin)
                    return;
            // process residuals block reducing the block size or the size of the simd vector
            constexpr unsigned newSimdVecLen = nItems > simdVecLen ? simdVecLen : simdVecLen / 2;
            if constexpr (newSimdVecLen > 0) {
               constexpr unsigned newNItems = newSimdVecLen;
               constexpr bool newOnlyOneIter = nItems / simdVecLen <= 2;
               Splitter<newNItems, newSimdVecLen, newOnlyOneIter>::loopblock(begin, end, args...);
            }
        }
    };

    // Wraps all arguments and delegate to the loop Splitter
    template<typename...Args>
    FORCE_INLINE static void fastloop(vec_size_t end, double *dst, Args...srcs)
    {
        constexpr vec_size_t block_size = 16;
        static_assert(block_size >= SimdMaxLen && (block_size % SimdMaxLen) == 0,
            "block_size must be a multiple of SimdMaxLen");
        vec_size_t begin = 0;
        Splitter<block_size, SimdMaxLen, false>::loopblock(begin, end, dst, ArgWrap<SimdMaxLen, Aligned, Args>(srcs)...);
    }
};


// convert booleans to logical masks (i.e. double)
template <typename T> static T toarg(T x) { return x; }
static double toarg(bool x) { return b2d(x); }

template <fun_enum F, unsigned VecLen, bool UseAVX, bool UseFMA>
struct LoopFun
{
    template <typename...Args>
    static void f(vec_size_t n, double *dst, Args... srcs)
    {
        // Always dispatch unaligned, as there is no
        // penalty in using unaligned load when argumemts are aligned
        // FIXME: In the special case where AVX512 is unaligned but AVX would be aligned,
        // we should dispatch using AVX, which is slightly faster
        LoopImpl<F, VecLen, false>::fastloop(n, dst, toarg(srcs)...);
#if defined(__AVX__) && (AVXONLY == 0)
        _mm256_zeroupper();
#endif
    }
};

template <fun_enum F, unsigned VecLen, bool UseAVX, bool UseFMA>
struct OuterFun; // to be specialized for every fun_enum with the include file below
#include "gen-vec-fun-body.h"

template <fun_enum C, unsigned VecLen, bool UseAVX, bool UseFMA>
inline constexpr bool instantiateFun()
{
    return !UseFMA      // not using FMA instructions: all functions must be instantiated
        || VecLen > 4   // AVX512 or higher: there is no separate case for non-FMA, hence all functions must be instantiated
        || (UseAVX && is_fma(fun_id<C>::id)); // VecLen<=4, using FMA, using AVX: instantiate only if it is a FMA function, otherwise there is no difference with the non FMA version and we do not need to intantiate again
}

template <fun_enum C, unsigned VecLen, bool UseAVX, bool UseFMA>
struct OuterPtrAssigner
{
    static void assign(OuterPtr &p)
    {
        if constexpr(instantiateFun<C, VecLen, UseAVX, UseFMA>())
            get<C>(p) = &OuterFun<C, VecLen, UseAVX, UseFMA>::f;
    }
};


template <unsigned VecLen, bool UseAVX, bool UseFMA>
void OuterPtrSet<VecLen, UseAVX, UseFMA>::init(OuterPtr& p)
{
#include "gen-vec-fun-init.h"
}
