#include "vec-names.h"
#define MCVEC_FUN(n,m) \
   OuterPtrAssigner<MCVEC_ENUMNAME(n,m),VecLen,UseAVX,UseFMA>::assign(p);
#define MCVEC_FUN1(n,m,...) MCVEC_FUN(n,m)
#define MCVEC_FUN2(n,m,...) MCVEC_FUN(n,m)
#define MCVEC_FUN3(n,m,...) MCVEC_FUN(n,m)

#include "auto/all-fun.h"
