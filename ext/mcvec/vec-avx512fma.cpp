#if (SKIP_AVX512 != 1) && !defined(_MSC_VER)

#if !defined(__AVX512DQ__) || !defined(__FMA__)
#error This file must be compiled with avx512dq and fma instructions
#endif

#include "vec-xxx.h"

template struct OuterPtrSet<8, true, true>; // force instantiation

#endif
