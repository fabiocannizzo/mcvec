#include "vec-names.h"
#define MCVEC_FUN(n,m) template <> inline MCVEC_LOOPTYPENAME(n, m)& get<MCVEC_ENUMNAME(n,m)>(LoopPtr& p) { return p.MCVEC_MEMBERNAME(n,m); }
#define MCVEC_FUN1(n,m,...) MCVEC_FUN(n,m)
#define MCVEC_FUN2(n,m,...) MCVEC_FUN(n,m)
#define MCVEC_FUN3(n,m,...) MCVEC_FUN(n,m)

#include "auto/all-loop.h"
